import React from 'react';
import {

  Table,
  Button,
  Card,
  Select,
  Popconfirm,
  Input,
  Icon,
  message,
  Row,
  Col
} from 'antd';
import { Link } from 'react-router-dom';
import api from '../../apis';

class EditableTable extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      data: props.dataSource,
      count: props.count,
      currentPagination: 1,
      customPagesize: 5
    };
    this.columns = props.columns.concat({
      title: 'Action',
      width: 200,
      fixed: 'right',
      align: 'center',
      render: record => (
        <>
          <Link style={{ color: '#33ff33' }} to={`/report/view/${record.id}`}>
            View
          <span style={{ letterSpacing: '20px' }}> </span>
          </Link>{' '}
        </>
      )
    });
  }
  //tableFunction
  onChange(pagination, filters, sorter) {
    console.log('params', pagination, filters, sorter);
  }

  cancel(e) {
    message.error('Click on No');
  }
  //filter
  PositionFilter(event) {
    let inputdata = event.target.value.toLowerCase();
    const master = this.props.dataSource;
    console.log('user type: ', inputdata);
    if (inputdata === '') {
      this.setState({
        data: master
      });
    } else {
      const clone = master.filter(item => {
        return Object.keys(item).some(key =>
          item[key]
            .toString()
            .toLowerCase()
            .includes(inputdata.toLowerCase())
        );
      });
      this.setState({
        data: clone
      });

      console.log('Matched', clone);
    }
  }
  isEditing = record => record.key === this.state.editingKey;

  delete = key => {
    const newData = [...this.state.data];
    const index = newData.findIndex(item => key === item.key);
    const item = newData[index];

    this.props.deleteData(item.id);
    // message.success('Deleted !');
  };

  componentDidUpdate(prevProps) {
    // Typical usage (don't forget to compare props):
    if (this.props.dataSource !== prevProps.dataSource) {
      this.setState({
        loading: false,
        data: this.props.dataSource,
        count: this.props.count
      });
    }
  }
  render() {
    const { data, neworedit, currentPagination, customPagesize } = this.state;

    const onChange = page => {
      this.setState({
        currentPagination: page
      });
    };
    const pageSizeHandler = value => {
      this.setState({
        customPagesize: value
      });
    };
    const columns = this.columns.map(col => {
      if (col.key === 'title' && neworedit === false) {
        col.editable = false;
      } else if (col.key === 'title' && neworedit === true) {
        col.editable = true;
      }
      if (!col.editable) {
        return col;
      }
      return {
        ...col,
        onCell: record => ({
          record,
          dataIndex: col.dataIndex,
          title: col.title,
          editing: this.isEditing(record)
        })
      };
    });
    return (
      <Card bordered={false}>
        <Row>
          <Col sm={24} md={24} style={{ paddingBottom: '10px' }}>
            <div
              style={{
                fontSize: '20px',
                fontWeight: '500'
              }}
            >
              <span>{this.props.title}</span>
              <Select
                onChange={pageSizeHandler}
                defaultValue={customPagesize.toString()}
                style={{
                  width: '115px',
                  float: 'right',
                  float: 'right',
                  marginLeft:'1%'
                }}
              >
                <Select.Option value="5">5 / page</Select.Option>
                <Select.Option value="10">10 / page</Select.Option>
                <Select.Option value="20">20 / page</Select.Option>
              </Select>
              <Input
                onChange={event => this.PositionFilter(event)}
                style={{ width: '20%', float: 'right' }}
                prefix={
                  <Icon type="search" style={{ color: 'rgba(0,0,0,.25)' }} />
                }
                placeholder="Search"
              />
            </div>
          </Col>
        </Row>
        <Row>
          <Col sm={24} md={24}>
            <div style={{ border: '1px solid #e8e8e8' }}>
              <Table
                key={data.key}
                pagination={{
                  onChange: onChange,

                  pageSize: Number(customPagesize),
                  position: 'bottom'
                }}
                loading={this.state.loading}
                dataSource={data}
                columns={columns}
                rowClassName="editable-row"
                scroll={{ x: 1500 }}
              />
            </div>
          </Col>
        </Row>
        <footer style={{ marginTop: '10px' }}></footer>
      </Card>
    );
  }
}
export default EditableTable;
