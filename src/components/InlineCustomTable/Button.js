import { Button } from 'antd';
import React from 'react';
import styles from '../../styles/custom.module.less';

class CreateButton extends React.Component {
  render() {
    return (
      <Button onClick={this.props.click} style={{color:'white',borderColor:'#395D42',width:'100px',backgroundColor:'#395D42'}}>
        {this.props.role?"Create New Role":"Create"}
      </Button>
    );
  }
}

export default CreateButton;
