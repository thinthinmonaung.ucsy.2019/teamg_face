import React from 'react'
import { Form, Input, Col, Row, Divider, Icon, Select, Button, Breadcrumb } from 'antd'
import { fetchAccount, putAccount, postAccount, deleteAccount } from '../../../actions/Account'
import api from 'apis'
import { noti } from 'utils/index'
import { connect } from "react-redux"
import history from '../../../router/history'
import PageHeaderWrapper from '../../../components/PageHeaderWrapper'
import { fetchEmployee } from '../../../actions/Employee';
import { fetchRole } from '../../../actions/Role';

class CreateEmployee extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			data: {
				employee_id:"",
				position_id:"",
				department_id:"",
				name: "",
				password: "",
				con_pas: "",
				created_by: "",
				updated_by: ""
			},
			employee: {
				employee_id:"",
				position_id:"",
				department_id:"",
				empcode: "",
				nric: "",
				email: "",
				phone: "",
			}
			,
			employeeList: [],
			prefix: "+95",
		}
	}

	componentDidMount() {
		this.getAllData();
	}
	getAllData() {
		this.props.fetchRole();
	}

	async componentWillMount() {
		const response = await api.get('employees');
		if (response && response.status === 200) {
			this.setState({ ...this.state, employeeList: response.data.data })
		}
	}

	handleChange = (e) => {
		this.setState({ data: { ...this.state.data, [e.target.name]: e.target.value } })
	}

	_handelChange = (data) => {
		console.log(data)
		let { employee } = this.state;
		employee.employee_id = data.id;
		employee.position_id=data.position_id;
		employee.department_id=data.department_id;
		employee.empcode = data.code;
		employee.email = data.email;
		employee.nric = data.nric;
		employee.phone = data.phone;
		this.setState({ ...this.state, employee });
	}



	handleSubmit = async e => {
		e.preventDefault();
		const { employee } = this.state;
		this.props.form.validateFieldsAndScroll((err, data) => {
			if (!err) {
				data = {
					...data, employee_id:employee.employee_id,position_id:employee.position_id,department_id:employee.department_id,ecode: employee.empcode, nirc: employee.nric, phone_number: employee.phone,
					email: employee.email,
					created_by: "admin", updated_by: ''
				}
				this.props.postAccount(data);
				history.push('/account')
				noti('success', 'Successfully!', 'Account has been created successfully.')
			} else {
				noti('error', 'Unsuccessfully!', 'Fail to Create.')
			}
		});
	};

	btnCancel = () => {
		history.push('/account/')
	}
	render() {
		const { getFieldDecorator } = this.props.form;
		const { Option } = Select;
		const { TextArea } = Input;
		const { data, employee } = this.state;
		const renderRole = (
			<Select style={{
				width: '300px',
				marginleft: '10px',
				display: 'inline-block'
			}} placeholder="Please select Role">
				{this.props.role.map(item => {
					return <Option value={item.name}>{item.name}</Option>
				})}
			</Select>
		)
		return (
			<div>
				<PageHeaderWrapper />

				<Form onSubmit={this.handleSubmit}>
					<div style={{ marginLeft: '25px' }}>
						<Row>
							<h2><Divider orientation="left"><Icon type="idcard" style={{ fontSize: '30px' }} /> Personsl Information</Divider></h2>
							<Col span={12}>
								<Form.Item label="Name">
									{getFieldDecorator('name', {
										rules: [
											{
												required: true,
												message: "Please Enter Your Name"
											},
										]
									})(<Input placeholder="Please Enter Name" style={{ width: '80%' }} name="name" />)}
								</Form.Item>
							</Col>

							<Col span={12}>
								<Form.Item label="NIRC">
									<Input placeholder="Please Enter NIRC" style={{ width: '80%' }} name="nirc"
										value={employee.nric} disabled />
								</Form.Item>
							</Col>
						</Row>
						<Row>
							<Col span={12}>
								<Form.Item style={{
									width: '80%'
								}} label="Employee Code:">
									<Select style={{
										width: '100%'
									}} placeholder="Please select Employee Code" onChange={this._handelChange}>
										{this.state.employeeList.map(item => {
											return <Option value={item}>{item.code}</Option>
										})}
									</Select>
								</Form.Item>
							</Col>

							<Col span={12}>
								<Form.Item label="Phone No">
									<Input placeholder="00 0000 0000" style={{ width: '80%' }} name="phone_number"
										value={employee.phone} disabled />
								</Form.Item>
							</Col>
						</Row>

						<Row>
							<Col span={12}>
								<Form.Item label="Email">
									<Input placeholder="Please Enter Email" style={{ width: '80%' }} name="email"
										value={employee.email} disabled />
								</Form.Item>
							</Col>
							<Col span={12}>
								<Form.Item style={{
									width: '60%'
								}} label="Role:">
									{getFieldDecorator('role', {
										rules: [{ required: true, message: 'Please select role' }],
									})
										(renderRole)
									}
								</Form.Item>
							</Col>

						</Row>

						<Row>
							<Col span={12}>
								<Form.Item label="Password">
									{getFieldDecorator('password', {
										rules: [
											{
												required: true,
												message: "Please Enter Password"
											},
										],
									})(<Input.Password placeholder=" Please Enter Password" style={{ width: '80%' }} name="password" />)}
								</Form.Item>
							</Col>

							<Col span={12}>
								<Form.Item label="Comfirm Password">
									{getFieldDecorator('con_pas', {
										rules: [
											{
												required: true,
												message: "Please Enter Comfirm Password"
											},
										],
									})(<Input.Password placeholder=" Please Enter Comfirm Password" style={{ width: '80%' }} name="con_pas" />)}
								</Form.Item>
							</Col>
						</Row>



						<Row>
							<Form.Item wrapperCol={{ span: 12 }} style={{ marginLeft: '35%' }}>
								<Button type="primary" htmlType="submit" style={{ backgroundColor: "green", color: "white", borderColor: 'green' }} >
									Submit
                      </Button>
								<Button type="primary" onClick={this.btnCancel} style={{ backgroundColor: "white", color: "black", marginLeft: "15px", borderColor: 'gray' }}>
									Cancel
                      </Button>
							</Form.Item>
						</Row>
					</div>
				</Form>
			</div>
		);
	}
}
const Account = Form.create({})(CreateEmployee)

function mapStateToProps(state) {
	return {
		lang: state.locale.lang,
		isSignedIn: state.auth.isSignedIn,
		roleid: state.auth.roleid,
		isloaded: state.loading.isloaded,
		account: state.account.list,
		employee: state.employee.list,
		role: state.role.list,

	};
}
export default connect(
	mapStateToProps,
	{ fetchAccount, fetchEmployee, fetchRole, putAccount, postAccount, deleteAccount }
)(Account);