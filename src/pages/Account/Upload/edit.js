import React from 'react'
import { Col, Form, Avatar, Button, Input, Row, Breadcrumb, Divider, Icon } from 'antd'
import history from '../../../router/history'
import { fetchAccount, putAccount, postAccount, deleteAccount } from '../../../actions/Account'
import { connect } from "react-redux"
import api from 'apis';
import moment from 'moment';
import { noti } from 'utils/index'

class Vedit extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            code: this.props.match.params.id,
            data: null
        }
    }

    componentDidMount() {
        this.getData();
    }

    async getData() {
        const response = await api.get(`account/${this.state.code}`);
        if (response && response.status == 200) {
            let data = response.data.data;
            this.setState({ data: data })
        }
    }

    handleChange = (e) => {
        this.setState({ data: { ...this.state.data, [e.target.name]: e.target.value } })
    }
    handleSubmit = () => {
        let { data } = this.state;

        data.updated_at = moment(data.updated_at).format('YYYY-MM-DD h:mm:ss')
        data.created_at = moment(data.created_at).format('YYYY-MM-DD h:mm:ss')

        //  api.put(`account/${data.id}`, data);
        this.props.putAccount(data, data.id);
        this.props.history.push('/account')
    }





    btnCancel = () => {
        history.push('/account/')
    }


    render() {
        const { data } = this.state;
        if (!data) {
            return <div> </div>      //data waiting meassage
        }

        const { TextArea } = Input;

        return (
            <div>
                <Breadcrumb style={{ fontSize: '13px', fontWeight: 'bold' }}>
                    <Breadcrumb.Item>
                        <a href="/">Dashboard</a>
                    </Breadcrumb.Item>
                    <Breadcrumb.Item>
                        <a href="/account">Account</a>
                    </Breadcrumb.Item>
                    <Breadcrumb.Item>
                        <a href="#" style={{ color: 'green' }}>Account Edit </a>
                    </Breadcrumb.Item>
                </Breadcrumb>
                <Form style={{ marginLeft: '5%' }} onSubmit={this.handleSubmit}>
                    <div style={{ marginBottom: '40px' }}></div>


                    <h2><Divider orientation="left"><Icon type="idcard" style={{ fontSize: '30px' }} /> Personsl Information</Divider></h2>


                    <div>
                        <Row style={{ marginBottom: '40px' }}>
                            <Col span={12}>
                                <h3>Name</h3>
                                <Input style={{ width: '60%' }} name="name"
                                    value={data.name}
                                    onChange={this.handleChange} />
                            </Col>
                            <Col span={12}>
                                <h3>NIRC</h3>
                                <Input style={{ width: '60%' }} name="nirc"
                                    value={data.nirc}
                                    onChange={this.handleChange} />
                            </Col>
                        </Row>
                        <Row style={{ marginBottom: '40px' }}>
                            <Col span={12}>
                                <h3>Employee Code</h3>
                                <Input style={{ width: '55%' }} name="ecode"
                                    value={data.ecode}
                                    onChange={this.handleChange} />
                            </Col>
                            <Col span={12}>
                                <h3>Phone Number</h3>
                                <Input style={{ width: '60%' }} name="phone_number"
                                    value={data.phone_number}
                                    onChange={this.handleChange} />
                            </Col>
                        </Row>
                        <Row style={{ marginBottom: '40px' }}>



                            <Col span={12}>
                                <h3>Email</h3>
                                <Input style={{ width: '60%' }} name="email"
                                    value={data.email}
                                    onChange={this.handleChange} />
                            </Col>

                        </Row>

                        <Row style={{ marginBottom: '40px' }}>

                            <Col span={12}>
                                <h3>Password</h3>
                                <Input.Password style={{ width: '60%' }} name="password"
                                    onChange={this.handleChange} />
                            </Col>
                            <Col span={12}>
                                <h3>Confirm Password</h3>
                                <Input.Password style={{ width: '60%' }} name="con_pas"
                                    onChange={this.handleChange} />
                            </Col>
                        </Row>



                        <Row style={{ marginLeft: '30%' }}>
                            <Form.Item wrapperCol={{ span: 24 }}>
                                <Button type="primary" htmlType="submit" onClick={this.handleSubmit} style={{ backgroundColor: "green", color: "white", borderColor: 'green' }} onClick={this.btnSubmit}>
                                    Update
                            </Button>
                                <Button type="primary" onClick={this.btnCancel} style={{ backgroundColor: "white", color: "black", marginLeft: "15px", borderColor: 'gray' }}>
                                    Cancel
                            </Button>
                            </Form.Item>
                        </Row>
                    </div>
                </Form>
            </div>
        );
    }
}


function mapStateToProps(state) {
    return {
        lang: state.locale.lang,
        isSignedIn: state.auth.isSignedIn,
        roleid: state.auth.roleid,
        isloaded: state.loading.isloaded,
        account: state.account.list,
    };
}
export default connect(
    mapStateToProps,
    { fetchAccount, putAccount, postAccount, deleteAccount }
)(Vedit);