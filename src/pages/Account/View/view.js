import React from 'react'
import { Col,Form,Button,Breadcrumb,Divider,Icon }from 'antd'
import history from '../../../router/history'
import api from 'apis';

class View extends React.Component{

    constructor(props){
        super(props);
        this.state = {
            code : this.props.match.params.id,
            data : null
        }
    }

    componentDidMount() {
        this.getData();
    }

    async getData() {
        const response = await api.get(`account/${this.state.code}`);
        if(response && response.status == 200){
            let data = response.data.data;
            this.setState({data: data})
        }
    }

    btnCancel=()=>{
    history.push('/account/')
    }

    btnEdit=()=>{
        history.push(`/account/upload/edit/${this.state.code}`)
        }

    render(){

        const { data } = this.state;
        if(!data){
            return <div>Data is waiting!!!!</div>      //data waiting meassage
        }
        console.log(data);

        return(
            <Form style={{marginLeft:'10px'}}>
                <Breadcrumb style={{fontSize:'13px',fontWeight:'bold',marginBottom:'20px'}}>
                    <Breadcrumb.Item>
                    <a href="/">Dashboard</a>
                    </Breadcrumb.Item>
                    <Breadcrumb.Item>
                    <a href="/account">Account</a>
                    </Breadcrumb.Item>
                    <Breadcrumb.Item>
                    <a href="#" style={{color:'green'}}>Account View </a>
                    </Breadcrumb.Item>
                </Breadcrumb>

                <Col span={24}>
                       <div style={{marginLeft:'2%',marginBottom:'50px'}}>
                       <h2><Divider orientation="left"><Icon type="idcard" style={{ fontSize: '30px' }} /> Personsl Information</Divider></h2>
                        </div>
                    
                </Col>


                <div style={{marginLeft:'10%'}}>
                    <div>
                    <Col span={8}>
                            <h3>Name</h3>
                            <p>{data.name}</p>
                        </Col>
                        <Col span={8}>
                            <h3>Employee Code</h3>
                            <p>{data.ecode}</p>
                        </Col>
                        <Col span={8}>
                            <h3>NIRC</h3>
                            <p>{data.nirc}</p>
                        </Col>
                        
                    </div>
                    <div style={{marginBottom:'100px'}}></div>
                    <div>
                        <Col span={8}>
                            <h3>Email</h3>
                            <p>{data.email}</p>
                        </Col>
                        <Col span={10}>
                            <h3>Phone Number</h3>
                            <p>{data.phone_number}</p> 
                        </Col>
                        
                    </div><br></br><br></br><br></br><br></br><br></br><br></br><br></br>
                    <div>
                        <Col span={8} style={{marginBottom:'100px'}}>
                            <h3>Password</h3>
                            <p>******</p>
                        </Col>
                        <Col span={10}  style={{align:'center'}}>
                            <h3>Confirm Password</h3>
                            <p>******</p>
                        </Col>
                    </div>

                   

                    <div>
                        <Form.Item wrapperCol={{ span: 24 }}>
                            <Button type="primary" onClick={this.btnEdit} style={{backgroundColor:"green",color:"white",borderColor:'green',padding:'0px 27px'}} onClick={this.btnEdit}>
                            Edit
                            </Button>
                            <Button type="primary" onClick={this.btnCancel} style={{backgroundColor:"white",color:"black",marginLeft:"15px",borderColor:'gray'}}>
                            Cancel
                            </Button>
                        </Form.Item>
                    </div>
                </div>
            </Form>
        );
    }
}

export default View;