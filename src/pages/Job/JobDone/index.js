import React from 'react'
import { Row, Col } from 'antd';
import { Card } from 'antd';
import { Divider, Breadcrumb, Button } from 'antd';
import { Link } from 'react-router-dom';
import api from 'apis';
import { fetchComplain } from '../../../actions/Complain'
import { fetchEmployee } from '../../../actions/Employee';
import { putSchedule } from '../../../actions/Schedule';
import history from '../../../router/history'

import { connect } from "react-redux";
const uuidv4 = require('uuid/v4');
const moment = require('moment');


class AssignSchedule extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            id: this.props.match.params.id,
            data: [],
        }
    }
    componentDidMount() {
        this.getData();
        this.props.fetchComplain()
        this.props.fetchEmployee()
    }

    async getData() {
        const response = await api.get(`schedules/${this.state.id}`);
        if (response && response.status == 200) {
            this.setState({ data: response.data.data })
        }
    }

    btnCancel = () => {
        history.push('/job')
    }
    render() {
        const data = this.state.data;
        const dataSource = this.props.complain;
        dataSource.map(d => {
            let uuid = uuidv4();
            d.key = uuid;
        })
        let dataService = this.props.employee;
        dataService.map(d => {
            let uuid = uuidv4();
            d.key = uuid;
        })

        const { working_hour, model_number, complain_number,
            fup_number,
            warranty_year,
            warranty_description,
            customer_phone,
            customer_name,
            name,
            distance,
            job_title,
            date,
            amount,
            complain_job_title,
            complain_description, location,job_status } = this.state.data;
            

            const btnjobstart = (e) => {
               this.setState({
                job_status:"Complete"
               });
               data.job_status="Complete"
               this.props.putSchedule(data, data.id);
               this.props.history.push('/job');
            }
        const columns = [
            {
                title: 'Model No',
                dataIndex: 'model_number',
                width: '200px',
                fixed: 'left'

            },
            {
                title: 'FUP No',
                dataIndex: 'fup_number',
                width: '20%',
            },
            {
                title: 'Complain No',
                dataIndex: 'complain_number',
                width: '20%',

            },
            {
                title: 'Date',
                dataIndex: 'date',
                width: '20%',

            },
            {
                title: 'Status',
                dataIndex: 'complain_status',
                width: '200px',
                fixed: 'right'

            },
            {
                title: 'Action',
                align: 'center',
                width: '200px',
                fixed: 'right',
                render: record => (
                    <>
                        <Link style={{ color: 'green', marginRight: '0.5em' }} to={"/assigntoschedule/machineview/" + record.id}>View</Link>
                    </>
                )
            }
        ];


        return (
            <div>
                <Breadcrumb style={{ fontSize: '13px', fontWeight: 'bold', marginBottom: '20px' }}>
                    <Breadcrumb.Item>
                        <a href="/">Dashboard</a>
                    </Breadcrumb.Item>
                    <Breadcrumb.Item>
                        <a href="/job">Job</a>
                    </Breadcrumb.Item>
                    <Breadcrumb.Item>
                        <a href="" style={{ color: 'green' }}>View</a>
                    </Breadcrumb.Item>
                </Breadcrumb>
                <Card title="Complain Detail" style={{ width: '95%', marginRight: '2.5%', marginTop: '5%', marginLeft: '2.5%', borderRadius: '14px' }}>
                    <Row>
                        <Col span={5} offset={1}>Complain Number : </Col>
                        <Col span={4}>{complain_number}</Col>
                        <Col span={6} offset={4}>Model Number : </Col>
                        <Col span={4}>{model_number}</Col>
                    </Row>
                    <br /><br />
                    <Row>
                        <Col span={5} offset={1}>Working hour : </Col>
                        <Col span={4}>{working_hour}</Col>
                        <Col span={6} offset={4}>Warranty Description : </Col>
                        <Col span={4}>{warranty_description}</Col>
                    </Row>
                    <br /><br />
                    <Row>
                        <Col span={5} offset={1}>FUP Number : </Col>
                        <Col span={4}>{fup_number}</Col>
                    </Row>
                    <br /><br />
                    <Row>
                        <Col span={5} offset={1}>Warranty Year : </Col>
                        <Col span={4}>{warranty_year}</Col>
                        <Col span={6} offset={4}>Customer Ph No : </Col>
                        <Col span={4}>{customer_phone}</Col>
                    </Row>
                    <br /><br />
                    <Row>
                        <Col span={5} offset={1}>Customer Name : </Col>
                        <Col span={4}>{customer_name}</Col>
                        <Col span={6} offset={4}>Location : </Col>
                        <Col span={4}>{location}</Col>
                    </Row>
                    <br /><br />
                    <Row>
                        <Col span={5} offset={1}>Distance : </Col>
                        <Col span={4}>{distance}</Col>
                        <Col span={6} offset={4}>Department : </Col>
                        <Col span={4}>{name}</Col>
                    </Row>
                    <br /><br />
                    <Row>
                        <Col span={5} offset={1}>Date:</Col>
                        <Col span={4}>{moment(date).format('YYYY-MM-DD')}</Col>
                        <Col span={6} offset={4}>Job Title:</Col>
                        <Col span={4}>{job_title}</Col>
                    </Row>
                    <br /><br />
                    <Row>
                        <Col span={5} offset={1}>Amount : </Col>
                        <Col span={4}>{amount}</Col>
                        <Col span={6} offset={4}>Description : </Col>
                        <Col span={4}>{complain_description}</Col>
                    </Row>
                    <br /><br />
                    <Row>
                        <Col span={5} offset={1}>Complain Job Title : </Col>
                        <Col span={4}>{complain_job_title}</Col>
                    </Row>

                </Card>
                <Card title="Schedule Detail" style={{ width: '95%', marginRight: '2.5%', marginTop: '5%', marginLeft: '2.5%', borderRadius: '14px' }}>
                    <Row>
                        <Col span={8}><h3>Job Code :</h3><p>{data.job_code}</p></Col>
                        <Col span={8}><h3>Job Status :</h3><p>{data.job_status}</p></Col>
                        <Col span={8}><h3>Job Title :</h3><p>{data.job_title}</p></Col>
                    </Row><br /><br />

                    <Row>
                        <Col span={8}><h3>Job Description :</h3><p>{data.job_description}</p></Col>
                        <Col span={8}><h3>Inspection :</h3><p>{data.inspection}</p></Col>
                        <Col span={8}><h3>Watching List :</h3><p>{data.watching_list}</p></Col>
                    </Row><br /><br />

                    <Row>
                        <Col span={8}><h3>Amount :</h3><p>{data.s_amount}</p></Col>
                        <Col span={16}><h3>Service Charge :</h3><p>{data.service_charge}</p></Col>
                    </Row><br /><br />

                    <Row>
                        <Col span={8}><h3>Start Date :</h3><p>{moment(data.sdate).format('YYYY-MM-DD')}</p></Col>
                        <Col span={16}><h3>End Date :</h3><p>{moment(data.edate).format('YYYY-MM-DD')}</p></Col>
                    </Row>
                </Card>
                <div style={{ width: '95%', marginRight: '2.5%', marginTop: '5%', marginLeft: '2.5%', borderRadius: '14px' }}>
                    <Button style={{ color: 'white', backgroundColor: 'green', padding: '0px 20px', borderColor: 'green' }} type="edit" onClick={btnjobstart}>Job Done</Button>
                    <Button type="primary" style={{ marginLeft: '2%', backgroundColor: "white", padding: '0px 27px', color: "black", borderColor: "gray" }} onClick={this.btnCancel} >
                        Cancel
                    </Button>
                </div>
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        lang: state.locale.lang,
        isSignedIn: state.auth.isSignedIn,
        roleid: state.auth.roleid,
        isloaded: state.loading.isloaded,
        complain: state.complain.list,
        employee: state.employee.list,
        schedule: state.schedule.list,
        //   servicemen: state.service.list
    };
}
export default connect(
    mapStateToProps,
    { fetchComplain, fetchEmployee ,putSchedule }
)(AssignSchedule);