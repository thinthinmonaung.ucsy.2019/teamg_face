import React from 'react'
import { Row, Col } from 'antd';
import { Table, Card, Form, Radio, Input, DatePicker, Button } from 'antd';
import { Divider, Breadcrumb } from 'antd';
import { Link } from 'react-router-dom';
import api from 'apis';
import { noti } from 'utils/index';
import history from '../../../router/history'
import { fetchComplain } from '../../../actions/Complain'
import { fetchEmployee } from '../../../actions/Employee';

import { connect } from "react-redux";
import './index.css';
const uuidv4 = require('uuid/v4');
const moment = require('moment');
const { RangePicker } = DatePicker;
const { TextArea } = Input;


class EditSchedule extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      id: this.props.match.params.id,
      data: [],
      selectedRowKeys: [],
      selectedid: [],
      currentPagination: 1,
      customPagesize: 5,
    }
  }

  componentDidMount() {
    this.getData();
    this.props.fetchComplain()
    this.props.fetchEmployee()
  }

  async getData() {
    const response = await api.get(`schedules/${this.state.id}`);
    if (response && response.status == 200) {
      this.setState({ data: response.data.data })
    }
    this.setInitialValues()

  }
  setInitialValues = () => {
    const data = this.state.data;
    const { form } = this.props;
    if (data)
      form.setFieldsValue({

        s_amount: data.s_amount,
        job_status: data.job_status,
        inspection: data.inspection,
        watching_list: data.watching_list,
        service_charge: data.service_charge,
        job_code: data.job_code,
        job_description: data.job_description,
        job_title: data.job_title,


      });
  };

  handleSubmit = e => {
    const selectedRowKeys = this.state.selectedRowKeys;
    const selectedid = this.state.selectedid;

    e.preventDefault();
    this.props.form.validateFieldsAndScroll((err, fieldsValue) => {
      if (!err) {
        const rangev = fieldsValue['range'];
        const emp_array = selectedid

        const emp_string = emp_array.join();
        const values = {
          job_code: fieldsValue.job_code,
          job_description: fieldsValue.job_description,
          job_title: fieldsValue.job_title,
          s_amount: fieldsValue.s_amount,
          job_status: fieldsValue.job_status,
          inspection: fieldsValue.inspection,
          watching_list: fieldsValue.watching_list,
          service_charge: fieldsValue.service_charge,
          sdate: rangev[0].format('YYYY/MM/DD'),
          edate: rangev[1].format('YYYY/MM/DD'),
          employee_name: emp_string
        }
        api.put(`schedules/${this.state.id}`, values).then((result) => {
          if (result) {
            this.props.history.push('/schedules')
            noti('success', 'Successfully!', 'Schedules have been upgraded successfully.')
          }
        })
      } else {
        noti('error', 'Unsuccessfully!', 'Fail to Create.')
      }
    });
  };

  onSelectChange = (selectedRowKeys, selectedRow) => {
    let did = selectedRow.map(r => r.name);

    this.setState({ selectedRowKeys, selectedid: did });
  };

  btnCancel = () => {
    history.push('/schedules')
  }
  render() {
    const { data, selectedRowKeys } = this.state;
    const { getFieldDecorator } = this.props.form;


    const dateFormat = 'YYYY/MM/DD';

    const rowSelection = {
      selectedRowKeys,
      onChange: this.onSelectChange,
      hideDefaultSelections: true,
      onSelection: this.onSelection,
    };
    const dataSource = this.props.complain;
    dataSource.map(d => {
      let uuid = uuidv4();
      d.key = uuid;
    })
    let dataService = this.props.employee;
    dataService.map(d => {
      let uuid = uuidv4();
      d.key = uuid;
    })

    const { working_hour, model_number, complain_number,
      fup_number,
      warranty_year,
      warranty_description,
      customer_phone,
      customer_name,
      name,
      distance,
      job_title,
      date,
      amount,
      complain_job_title,
      complain_description, location } = this.state.data;

    const columns = [
      {
        title: 'Model No',
        dataIndex: 'model_number',
        width: 200,
        fixed: 'left'

      },
      {
        title: 'FUP No',
        dataIndex: 'fup_number',
        width: '30%'
      },
      {
        title: 'Complain No',
        dataIndex: 'complain_number',
        width: '30%',

      },
      {
        title: 'Date',
        dataIndex: 'date',
        width: '30%',

      },
      {
        title: 'Status',
        dataIndex: 'complain_status',
        width: 100,
        fixed: 'right'

      },
    ];
    const empcolumns = [
      {
        title: 'Name',
        width:200,
        dataIndex: 'name',
        key:'name',
        align:'center',
        fixed:'left'
      },
      {
        title: 'Position',
        dataIndex: 'posname',
        key:'1',
        align:'center'
      },
      {
        title: 'Phone No',
        dataIndex: 'phone',
        key:'2',
        align:'center'
      },
      {
        title: 'Address',
        dataIndex: 'parmanent_address',
        key:'3',
        align:'center'
      },
      {
        title: 'NRIC',
        dataIndex: 'nric',
        key:'4',
        align:'center'
      },
      {
        title: 'Employee Code',
        dataIndex: 'code',
        key:'5',
        align:'center'
      },

      {
        title: 'Action',
        key:'operation',
        align:'center',
        fixed:'right',
        width:200,
        render: record => (
          <>
            <Link style={{ color: 'green', marginRight: '0.5em' }} to={"/employees/detail/" + record.id}>View</Link>
          </>
        )
      },
    ];

    return (
      <div>
        <Breadcrumb style={{ fontSize: '13px', fontWeight: 'bold', marginBottom: '20px' }}>
          <Breadcrumb.Item>
            <a href="/">Configuration</a>
          </Breadcrumb.Item>
          <Breadcrumb.Item>
            <a href="/schedules">Schedule</a>
          </Breadcrumb.Item>
          <Breadcrumb.Item>
            <a href="" style={{ color: 'green' }}>Edit</a>
          </Breadcrumb.Item>
        </Breadcrumb>
        <Card title="Complain Information" style={{ width: '95%', marginRight: '2.5%', marginTop: '5%', marginLeft: '2.5%', borderRadius: '14px' }}>
          <Row>
            <Col span={5} offset={1}>Complain Number : </Col>
            <Col span={4}>{complain_number}</Col>
            <Col span={6} offset={4}>Model Number : </Col>
            <Col span={4}>{model_number}</Col>
          </Row>
          <br /><br />
          <Row>
            <Col span={5} offset={1}>Working hour : </Col>
            <Col span={4}>{working_hour}</Col>
            <Col span={6} offset={4}>Warranty Description : </Col>
            <Col span={4}>{warranty_description}</Col>
          </Row>
          <br /><br />
          <Row>
            <Col span={5} offset={1}>FUP Number : </Col>
            <Col span={4}>{fup_number}</Col>
          </Row>
          <br /><br />
          <Row>
            <Col span={5} offset={1}>Warranty Year : </Col>
            <Col span={4}>{warranty_year}</Col>
            <Col span={6} offset={4}>Customer Ph No : </Col>
            <Col span={4}>{customer_phone}</Col>
          </Row>
          <br /><br />
          <Row>
            <Col span={5} offset={1}>Customer Name : </Col>
            <Col span={4}>{customer_name}</Col>
            <Col span={6} offset={4}>Location : </Col>
            <Col span={4}>{location}</Col>
          </Row>
          <br /><br />
          <Row>
            <Col span={5} offset={1}>Distance : </Col>
            <Col span={4}>{distance}</Col>
            <Col span={6} offset={4}>Department : </Col>
            <Col span={4}>{name}</Col>
          </Row>
          <br /><br />
          <Row>
            <Col span={5} offset={1}>Date:</Col>
            <Col span={4}>{moment(date).format('YYYY-MM-DD')}</Col>
            <Col span={6} offset={4}>Job Title:</Col>
            <Col span={4}>{job_title}</Col>
          </Row>
          <br /><br />
          <Row>
            <Col span={5} offset={1}>Amount : </Col>
            <Col span={4}>{amount}</Col>
            <Col span={6} offset={4}>Description : </Col>
            <Col span={4}>{complain_description}</Col>
          </Row>
          <br /><br />
          <Row>
            <Col span={5} offset={1}>Complain Job Title : </Col>
            <Col span={4}>{complain_job_title}</Col>
          </Row>

        </Card>
        <div style={{ width: '95%', marginRight: '2.5%', marginTop: '5%', marginLeft: '2.5%', borderRadius: '14px' }}>
        </div>

        <div style={{ width: '85%', margin: ' 6% 13% 0 2%', borderRadius: '14px' }}>
          <div>
            <Form onSubmit={this.handleSubmit} >
              <h4 style={{ marginBottom: '2.5%' }}>You can assign your schedule Information.</h4>
              <Row>
                <Col span={24}>
                  <Form.Item label='Interval Time :' style={{ width: '50%' }}>

                    {getFieldDecorator(['range'], {
                      rules: [{
                        type: 'array',
                        required: true,
                        message: 'Please input your range!'
                      }]
                    })(<RangePicker
                      format={dateFormat} />)}
                  </Form.Item>
                </Col>
              </Row>
              <Row>
                <Col span={12}>
                  <Form.Item label='Service Charge:' style={{ width: '60%' }}>

                    {getFieldDecorator('service_charge', {
                      rules: [{
                        required: true,
                        message: 'Please input Service Charge!'
                      }]
                    })(<Input addonAfter="Kyats" />)}
                  </Form.Item>
                </Col>
                <Col span={12}>
                  <Form.Item label='Amount:' style={{ width: '60%' }}>

                    {getFieldDecorator('s_amount', {
                      rules: [{
                        required: true,
                        message: 'Please input your amount!'
                      }]
                    })(<Input addonAfter="Kyats" />)}
                  </Form.Item>
                </Col>
              </Row>
              <Row>
                <Col span={12}>
                  <Form.Item label='Inspection:' style={{ width: '60%' }}>

                    {getFieldDecorator('inspection')(<Radio.Group>
                      <Radio value='Yes'>Yes</Radio>
                      <Radio value='No'>No</Radio>
                    </Radio.Group>)}
                  </Form.Item>
                </Col>
                <Col span={12}>
                  <Form.Item label='Watching List:' style={{ width: '60%' }}>
                    {getFieldDecorator('watching_list')(<Radio.Group>
                      <Radio value='Yes'>Yes</Radio>
                      <Radio value='No'>No</Radio>
                    </Radio.Group>)}
                  </Form.Item>
                </Col>
              </Row>
              <Row>
                <Col span={12}>
                  <Form.Item label='Job Code:' style={{ width: '60%' }}>

                    {getFieldDecorator('job_code', {
                      rules: [{
                        required: true,
                        message: 'Please input Job Code!'
                      }]
                    })(<Input placeholder="Enter Code" />)}
                  </Form.Item>
                </Col>
                <Col span={12}>
                  <Form.Item label='Job Status:' style={{ width: '60%' }}>

                    {getFieldDecorator('job_status', {
                      rules: [{
                        required: true,
                        message: 'Please input Job Status!'
                      }]
                    })(<Input placeholder="Enter Status" disabled />)}
                  </Form.Item>
                </Col>
              </Row>
              <Row>
                <Col span={12}>
                  <Form.Item label='Job Title:' style={{ width: '60%' }}>

                    {getFieldDecorator('job_title', {
                      rules: [{
                        required: true,
                        message: 'Please input Job title!'
                      }]
                    })(<Input placeholder="Enter Title" />)}
                  </Form.Item>
                </Col>
                <Col span={12}>
                  <Form.Item label='Job Description:' style={{ width: '60%' }}>

                    {getFieldDecorator('job_description', {
                      rules: [{
                        required: true,
                        message: 'Please input Job Description!'
                      }]
                    })(<TextArea rows={2} placeholder='Enter Description' />)}
                  </Form.Item>
                </Col>
              </Row>
              <Card style={{ width: '110%', borderRadius: '14px',marginBottom:'3%' }}>
                <Table
                  key={data.key}
                  rowSelection={rowSelection}
                  dataSource={dataService}
                  columns={empcolumns}
                  rowClassName="editable-row"
                  title={() => <h2>Service Man Information</h2>}
                  scroll={{ x: 1500 }}
                />
              </Card>
              <Button htmlType="submit" style={{ color: 'white', backgroundColor: 'green', padding: '0px 25px', borderColor: 'green', marginRight: '3%' }}>Submit</Button>
              <Button style={{ color: 'gray', backgroundColor: 'white', padding: '0px 25px', borderColor: 'gray' }} onClick={this.btnCancel}>Cancel</Button>
            </Form>
          </div>

        </div>

      </div>
    );
  }
}


const Schedule = Form.create()(EditSchedule);

function mapStateToProps(state) {
  return {
    lang: state.locale.lang,
    isSignedIn: state.auth.isSignedIn,
    roleid: state.auth.roleid,
    isloaded: state.loading.isloaded,
    position: state.position.list,
    complain: state.complain.list,
    employee: state.employee.list
  };
}

export default connect(
  mapStateToProps,
  { fetchEmployee, fetchComplain }
)(Schedule);