import React from 'react';
//component
import PageHeaderWrapper from '../../components/PageHeaderWrapper';
import ScrollTable from './CustomScrollTable';
import Can from '../../../src/utils/Can';
import Forbidden from '../Forbidden';
import { fetchSchedules, fetchSchedule, putSchedule, postSchedule, deleteSchedule } from '../../actions/Schedule';
import { connect } from "react-redux";

const uuidv4 = require('uuid/v4');


class Schedule extends React.Component {

  constructor(props) {
    super(props);
    this.state = {

    }
  }

  componentDidMount() {
    this.getAllSchedule();
  }

  getAllSchedule() {
    this.props.fetchSchedules()
  }
  getSchedule(id) {
    this.props.fetchSchedule(id);
  }

  //update Schedule
  editSchedule = (data, id) => {
    this.props.putSchedule(data, id);
  }

  // to create new Schedule
  createNewSchedule = (data) => {
    data.created_by = "admin";
    data.updated_by = '';
    this.props.postSchedule(data);
  }

  // to delete Schedule
  deleteSchedule = (id) => {
    this.props.deleteSchedule(id);
  }

  render() {
    const { selectedRowKeys } = this.state;
    const dataSource = this.props.schedules

    const columns = [
      {
        title: 'Job Code',
        dataIndex: 'job_code',
        key: 'job_code',
        width: 150,
        sorter: (a, b) => a.job_code.length - b.job_code.length,
        sortDirections: ['ascend', 'descend'],
        fixed: 'left',
        align:'center',

      },
      {
        title: 'Complain Number',
        dataIndex: 'complain_number',
        key: '1',
        sorter: (a, b) => a.complain_number.length - b.complain_number.length,
        sortDirections: ['ascend', 'descend'],
        align:'center',
      },
      {
        title: 'Service Man',
        dataIndex: 'employee_name',
        key: '2',
        sorter: (a, b) => a.employee_name.length - b.employee_name.length,
        sortDirections: ['ascend', 'descend'],
        align:'center',
      },
      {
        title: 'Start Date',
        dataIndex: 'sdate',
       key:'3',
       align:'center',
      },
      {
        title: 'End Date',
        dataIndex: 'edate',
        key:'4',
        align:'center',
      },
      {
        title: 'Status',
        dataIndex: 'job_status',
        width: 110,
        align:'center',
        key:'job_status',
        fixed: 'right'
      },

    ];
    const perform = {
      create: "position:create",
      edit: "position:edit"
    }
    const newData = {
      title: "",
      description: ""
    }

    let data = this.props.schedules;
    console.log(data)
    data.map(d => {
      let uuid = uuidv4();
      d.key = uuid;
      console.log(d)
    })

    return (
      <div>
        <Can
          role="Admin"
          perform="position:list"
          no={() => {
            return <Forbidden />
          }}
        >
          <PageHeaderWrapper />
          <ScrollTable
            dataSource={dataSource}
            columns={columns}
            title="Schedule List"
            role="Admin"
            perform={perform}
            newData={newData}
            getData={this.getAllSchedule}
            getdata={(id) => this.getSchedule(id)}
            editData={(data, id) => this.editSchedule(data, id)}
            createNewData={(data) => this.createNewSchedule(data)}
            deleteData={(id) => this.deleteSchedule(id)}
          />
        </Can>
      </div>
    );
  }
}
function mapStateToProps(state) {
  return {
    lang: state.locale.lang,
    isSignedIn: state.auth.isSignedIn,
    roleid: state.auth.roleid,
    isloaded: state.loading.isloaded,
    schedules: state.schedule.list,
  };
}
export default connect(
  mapStateToProps,
  { fetchSchedules, fetchSchedule, putSchedule, postSchedule, deleteSchedule }
)(Schedule);
