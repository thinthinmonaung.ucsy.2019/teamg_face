import React from 'react';
import { Card } from 'antd';
import { Button, Icon, Breadcrumb } from 'antd';
import { Link } from 'react-router-dom';
import { Row, Col } from 'antd';
import api from 'apis';
import history from '../../router/history'
const moment = require('moment');

class Complain extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            id: this.props.match.params.id,
            data: []
        }
    }

    componentDidMount() {
        this.getData();
    }

    async  getData() {
        const response = await api.get(`complains/${this.state.id}`);
        if (response && response.status == 200) {
            let data = response.data.data;

            this.setState({ data: data })

        }
    }
    btnCancel = () => {
        history.push('/assign to schedule')
    }
    render() {
        const { working_hour, model_number, complain_number,
            fup_number,
            warranty_year,
            warranty_description,
            customer_phone,
            customer_name,
            name,
            distance,
            job_title,
            date, prefix,
            machine_serial_number,
            engine_serial_number,
            amount,
            complain_description, location,complain_job_title } = this.state.data;

        return <div>
            <Breadcrumb style={{ fontSize: '13px', fontWeight: 'bold', marginBottom: '5%' }}>
                <Breadcrumb.Item>
                    <a href="/">Configuration</a>
                </Breadcrumb.Item>
                <Breadcrumb.Item>
                    <a href="/assign to schedule">Assign to Schedule</a>
                </Breadcrumb.Item>
                <Breadcrumb.Item>
                    <a href="" style={{ color: 'green' }}>Machine History View</a>
                </Breadcrumb.Item>
            </Breadcrumb>
            <Card title='Complain Information' style={{ width: '95%', marginRight: '2.5%', marginLeft: '2.5%', borderRadius: '5px' }}>
                <div style={{ marginLeft: '3%' }}>
                    <Row>
                        <Col span={9}><h3>Complain Number:</h3><p>{complain_number}</p></Col>
                        <Col span={8}><h3>Model number:</h3><p>{model_number}</p></Col>
                        <Col span={7}><h3>Warranty hour:</h3><p>{warranty_year}</p></Col>
                    </Row>
                    <br /><br />

                    <Row>
                        <Col span={9}><h3>Warranty description:</h3><p>{warranty_description}</p></Col>
                        <Col span={8}><h3>FUP number:</h3><p>{fup_number}</p></Col>
                        <Col span={7}><h3>Customer Ph No:</h3><p>{prefix}{customer_phone}</p></Col>
                    </Row>
                    <br /><br />

                    <Row>
                        <Col span={9}><h3>Working hour:</h3><p>{working_hour}</p></Col>
                        <Col span={8}><h3>Customer Name:</h3><p>{customer_name}</p></Col>
                        <Col span={7}><h3>Location:</h3><p>{location}</p></Col>
                    </Row>
                    <br /><br />

                    <Row>
                        <Col span={9}><h3>Distance:</h3><p>{distance}</p></Col>
                        <Col span={8}><h3>Department:</h3><p>{name}</p></Col>
                        <Col span={7}><h3>Date:</h3><p>{moment(date).format('YYYY-MM-DD')}</p></Col>
                    </Row>
                    <br />
                    <br />

                    <Row>
                        <Col span={9}><h3>Job Title:</h3><p>{job_title}</p></Col>
                        <Col span={8}><h3>Amount</h3><p>{amount}</p></Col>
                        <Col span={7}><h3>Description:</h3><p>{complain_description}</p></Col>
                    </Row>
                    <br />
                    <br />
                    <Row>
                        <Col span={24}><h3>Complain Job Title:</h3><p>{complain_job_title}</p></Col>

                    </Row>
                </div>
            </Card>
            <Card title='Machine Information' style={{ width: '95%',marginTop:'2.5%', marginRight: '2.5%', marginLeft: '2.5%', borderRadius: '5px' }}>
                    <Row>
                        <Col span={6}>Model Number:</Col>
                        <Col span={4}>{model_number}</Col>
                        <Col span={6} offset={4}>FUP Number:</Col>
                        <Col span={4}>{fup_number}</Col>

                    </Row>
                    <br />
                    <br/>
                    <Row>
                        <Col span={6}>Machine Serial Number:</Col>
                        <Col span={4}>{machine_serial_number}</Col>
                        <Col span={6} offset={4}>Engine Serial Number:</Col>
                        <Col span={4}>{engine_serial_number}</Col>
                    </Row>
                    <br />
                    <br/>
                    <Row>
                        <Col span={6}>Warranty Year:</Col>
                        <Col span={4}>{warranty_year}</Col>
                        <Col span={6} offset={4}>Working Hour:</Col>
                        <Col span={4}>{working_hour}</Col>
                    </Row>
                    <br/>
                    <br/>
                    <Row>
                    <Col span={6}>Warranty Description:</Col>
                    <Col span={4}>{warranty_description}</Col>
                    </Row>
                    <br />
                </Card>
           <Button type="primary" style={{ marginLeft: '2%', backgroundColor: "green",width:'120px',height:'35px',marginTop:'2.5%', color: "white", borderColor: "gray" }} onClick={this.btnCancel} >
                Back
                    </Button>
        </div>
    }
}
export default Complain;