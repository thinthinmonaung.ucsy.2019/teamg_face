import { Table } from 'antd';
import React from 'react';
import { Link } from 'react-router-dom'
import { DatePicker, Divider } from 'antd';
import { Radio, Button } from 'antd';
import { Row, Col, Form, Input } from 'antd';
import api from 'apis';
import history from '../../../router/history'
import { noti } from 'utils/index';
//import moment from 'moment';
const uuidv4 = require('uuid/v4');
const { RangePicker } = DatePicker;
const { TextArea } = Input;


const columns = [
  {
    title: 'Name',
    dataIndex: 'name',
  },
  {
    title: 'Position',
    dataIndex: 'posname'
  },
  {
    title: 'Phone No',
    dataIndex: 'phone',
  },
  {
    title: 'Address',
    dataIndex: 'parmanent_address',
  },
  {
    title: 'NRIC',
    dataIndex: 'nric',
  },
  {
    title: 'Employee Code',
    dataIndex: 'code',
  },

  {
    title: 'Action',
    render: record => (
      <>
        <Link style={{ color: 'green', marginRight: '0.5em' }} to={"/employees/detail/" + record.id}>View</Link>
      </>
    )
  },
];


class As extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      data: [],
      complain_id: props.complain_id,
      count: props.count,
      currentPagination: 1,
      customPagesize: 5,
      selectedRowKeys: [],
      selectedid: []

    };

  }
  handleSubmit = e => {
    const selectedRowKeys = this.state.selectedRowKeys;
    const selectedid = this.state.selectedid;

    e.preventDefault();
    this.props.form.validateFieldsAndScroll((err, fieldsValue) => {
      if (!err) {
        const rangev = fieldsValue['range'];
        const emp_array = selectedid
        console.log(emp_array);
        
        const emp_string = emp_array.join();
        const values = {
          complain_id: this.state.complain_id,
          job_code: fieldsValue.job_code,
          job_description: fieldsValue.job_description,
          job_title: fieldsValue.job_title,
          s_amount: fieldsValue.s_amount,
          job_status: fieldsValue.job_status,
          inspection: fieldsValue.inspection,
          watching_list: fieldsValue.watching_list,
          service_charge: fieldsValue.service_charge,
          sdate: rangev[0].format('YYYY/MM/DD'),
          edate: rangev[1].format('YYYY/MM/DD'),
          employee_name: emp_string
        }
        console.log("Test", values);
         api.post(`schedules`, values).then((result) => {
            if(result) {
              //  this.props.form.resetFields();
              history.push('/assign to schedule')
              noti('success', 'Successfully!', 'Schedules have been created successfully.')
            }
        })
      } else {
        noti('error', 'Unsuccessfully!', 'Fail to Create.')
      }
    });
  };
  async postData(v) {
    api.post('schedules', v);
  }

  onSelectChange = (selectedRowKeys, selectedRow) => {
    let did = selectedRow.map(r => r.name);
    this.setState({ selectedRowKeys, selectedid: did });
  };
  componentDidUpdate(prevProps) {
    if (this.props.dataSource !== prevProps.dataSource) {
      this.setState({
        loading: false,
        data: this.props.dataSource,
        count: this.props.count
      });
    }
  }
  btnCancel = () => {
    history.push('/assign to schedule')
  }

  render() {
    const { getFieldDecorator } = this.props.form;
    const { data, selectedRowKeys } = this.state;


    const dateFormat = 'YYYY/MM/DD';
    const rowSelection = {
      selectedRowKeys,
      onChange: this.onSelectChange,
      hideDefaultSelections: true,

      onSelection: this.onSelection,
    };
    return (
      <div>
        <Form onSubmit={this.handleSubmit} >
          <Table
            key={data.key}
            rowSelection={rowSelection}
            dataSource={data}
            columns={columns}
            title={() => <h2>Service Man Information</h2>}
            rowClassName="editable-row"
            bordered
          />
        </Form>
      </div>);
  }
}
const SelectTable = Form.create()(As);
export default SelectTable;
