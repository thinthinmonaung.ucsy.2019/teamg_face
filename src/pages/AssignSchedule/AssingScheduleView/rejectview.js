import React from 'react'
import { Row, Col } from 'antd';
import { Card } from 'antd';
import { Divider } from 'antd';
import ScrollTable from '../../ScrollTable/CustomScrollTable';
import { Button, Table, Breadcrumb,Popconfirm } from 'antd';
import { Link } from 'react-router-dom';
import api from 'apis';
import history from '../../../router/history'
import { connect } from "react-redux";
import { fetchComplain } from '../../../actions/Complain'
import '../index.css';
const moment = require('moment');
const uuidv4 = require('uuid/v4');



class AssignSchedule extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            id: this.props.match.params.id,
            data: []
        }
    }

    componentDidMount() {
        this.getAllComplain()
        this.getData();
    }


    getAllComplain() {
        this.props.fetchComplain()
    }
    async getData() {
        const response = await api.get(`complains/${this.state.id}`);
        if (response && response.status == 200) {
            this.setState({ data: response.data.data })
        }

    }

    btnGoBack=()=>{
        history.push('/assign to schedule')
    }
    render() {
        const reject = async(id) => {
            api.put(`complains/reject/${id}`).then(result => 
                this.getAllComplain()
            )
        
        }
        const { working_hour, model_number, complain_number,
            fup_number,
            warranty_year,
            warranty_description,
            customer_phone,
            customer_name,
            name,
            distance,
            job_title,
            date, prefix,
            amount,
            complain_description, location,complain_job_title } = this.state.data;
        const columns = [
            {
                title: 'Model No',
                dataIndex: 'model_number',
                width: 200,
                key:'model_number',
                align:'center',
                fixed:'left'

            },
            {
                title: 'FUP No',
                dataIndex: 'fup_number',
               key:'1',
               align:'center'
            },
            {
                title: 'Complain No',
                dataIndex: 'complain_number',
                key:'2',
                align:'center'
            },
            {
                title: 'Date',
                dataIndex: 'date',
                key:'3',
                align:'center'

            },
            {
                title: 'Status',
                dataIndex: 'complain_status',
                width: 120,
                key:'complain_status',
                align:'center',
                fixed:'right'
            },
            {
                title: 'Action',
                key:'operation',
                align: 'center',
                width: 120,
                fixed:'right',
                render: record => (
                  <>
                     <Link style={{ color: 'green',marginRight:'0.5em' }} to={"/assigntoschedule/machineview/" + record.id}>View</Link>
                  </>
                )
              }
        ];

        const dataSource = this.props.complain;
        console.log("Data", dataSource);

        dataSource.map(d => {
            let uuid = uuidv4();
            d.key = uuid;
        })
        return (
            <div>
                <Breadcrumb style={{ fontSize: '13px', fontWeight: 'bold', marginBottom: '5%' }}>
                    <Breadcrumb.Item>
                        <a href="/">Configuration</a>
                    </Breadcrumb.Item>
                    <Breadcrumb.Item>
                        <a href="/assign to schedule">Assign to Schedule</a>
                    </Breadcrumb.Item>
                    <Breadcrumb.Item>
                        <a href="" style={{ color: 'green' }}>View</a>
                    </Breadcrumb.Item>
                </Breadcrumb>
                <Card title="Complain Information" style={{ width: '95%', marginRight: '2.5%', marginBottom: '4%',marginLeft: '2.5%', borderRadius: '14px' }}>
                <div>
                    <Row>
                        <Col span={9}><h3>Complain Number:</h3><p>{complain_number}</p></Col>
                        <Col span={8}><h3>Model number:</h3><p>{model_number}</p></Col>
                        <Col span={7}><h3>Warranty hour:</h3><p>{warranty_year}</p></Col>
                    </Row>
                    <br /><br />

                    <Row>
                        <Col span={9}><h3>Warranty description:</h3><p>{warranty_description}</p></Col>
                        <Col span={8}><h3>FUP number:</h3><p>{fup_number}</p></Col>
                        <Col span={7}><h3>Customer Ph No:</h3><p>{prefix}{customer_phone}</p></Col>
                    </Row>
                    <br /><br />

                    <Row>
                        <Col span={9}><h3>Working hour:</h3><p>{working_hour}</p></Col>
                        <Col span={8}><h3>Customer Name:</h3><p>{customer_name}</p></Col>
                        <Col span={7}><h3>Location:</h3><p>{location}</p></Col>
                    </Row>
                    <br /><br />

                    <Row>
                        <Col span={9}><h3>Distance:</h3><p>{distance}</p></Col>
                        <Col span={8}><h3>Department:</h3><p>{name}</p></Col>
                        <Col span={7}><h3>Date:</h3><p>{moment(date).format('YYYY-MM-DD')}</p></Col>
                    </Row>
                    <br />
                    <br />

                    <Row>
                        <Col span={9}><h3>Job Title:</h3><p>{job_title}</p></Col>
                        <Col span={8}><h3>Amount</h3><p>{amount}</p></Col>
                        <Col span={7}><h3>Description:</h3><p>{complain_description}</p></Col>
                    </Row>
                    <br />
                    <br />
                    <Row>
                        <Col span={24}><h3>Complain Job Title:</h3><p>{complain_job_title}</p></Col>

                    </Row>
                </div>
                </Card>
                <div style={{ width: '95%', marginRight: '2.5%', marginLeft: '2.5%', borderRadius: '14px' }}>
                    <ScrollTable
                        dataSource={dataSource}
                        columns={columns}
                        title="Machine History"
                    /><Button onClick={this.btnGoBack} style={{ marginTop: '2.5%', backgroundColor: "green", color: "white", borderColor: "green" }}>Go Back</Button>
        
                </div>

            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        lang: state.locale.lang,
        isSignedIn: state.auth.isSignedIn,
        roleid: state.auth.roleid,
        isloaded: state.loading.isloaded,
        complain: state.complain.list,
    };
}
export default connect(
    mapStateToProps,
    { fetchComplain }
)(AssignSchedule);