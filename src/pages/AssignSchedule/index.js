import React from 'react';
import { Table, Divider, Input, Breadcrumb, Card, Row, Col, Icon, Select, Popconfirm } from 'antd';
import { connect } from "react-redux";
import { Link, Route } from 'react-router-dom'
import api from 'apis';
import history from '../../router/history'
import ScrollTable from '../ScrollTable/CustomScrollTable';
import { fetchComplain } from '../../actions/Complain';
import PageHeaderWrapper from '../../components/PageHeaderWrapper'
const uuidv4 = require('uuid/v4');

const renderCom = (record) => {
    return (<Link
        to={"/assign to schedule/rejectview/" + record.id}
        style={{ color: 'green' }}>
        View
    </Link>
    );
}
const renderCom1 = (record) => {
    return (<span> <Link
        to={"/assign to schedule/detail/" + record.id}
        style={{ color: 'green' }}>
        View
        </Link>
        <Divider type="vertical" />
        <Link
            to={"/assign to schedule/accept/" + record.id}
            style={{ color: 'blue' }}>
            Accept
        </Link>
        <Divider type="vertical" />
        <Popconfirm
            title="Are you sure reject?"
            onConfirm={() => reject(record.id)}
            okType="danger"
        >
            <a style={{ color: '#ff3333' }}>Reject</a>
        </Popconfirm>

    </span>
    );
}
const reject = async(id) => {
    api.put(`complains/reject/${id}`).then(result => 
        window.location.reload(false)
    )
}

const columns = [
    {
        title: 'Complain Job Title',
        dataIndex: 'complain_job_title',
        key:'complain_job_title',
        align: 'center',
        width: 200,
        sorter: (a, b) => a.complain_job_title.length - b.complain_job_title.length,
        sortDirections: ['ascend', 'descend'],
        fixed: 'left'
    },
    {
        title: 'Customer Name',
        dataIndex: 'customer_name',
        key:'1',
        align: 'center',
        sorter: (a, b) => a.customer_name.length - b.customer_name.length,
        sortDirections: ['ascend', 'descend']
    },
    {
        title: 'Complain No',
        dataIndex: 'complain_number',
        key:'2',
        align: 'center',
    },
    {
        title: 'Date',
        dataIndex: 'date',
        key:'3',
        align: 'center',
     },
    {
        title: 'Location',
        dataIndex: 'location',
        key:'4',
        align: 'center',
    },
    {
        title: 'Status',
        dataIndex: 'complain_status',
        key:'complain_status',
        align: 'center',
        width: 120,
        fixed:'right'
    },
    {
        title: 'Action',
        key: 'operation',
        align: 'center',
        width:200,
        render: (text, record) => (
            <span>
                {record.complain_status === 'Rejected' ? (renderCom(record)) : (renderCom1(record))}
            </span>
        ),
        fixed: 'right'
    }
];
class AssignSchedule extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            currentPagination: 1,
            customPagesize: 5
        };
    }

    componentDidMount() {
        this.getAllComplain();
    }

    getAllComplain() {
        console.log(this.props.fetchComplain())
    }
    reject = key => {
        const newData = this.props.complains;
        const index = newData.findIndex(item => key === item.key);
        const item = newData[index];

        this.deleteEmp(item.id);
    };

    deleteEmp = (id) => {
        console.log(id);
        ;
    }

    render() {
        const { currentPagination, customPagesize } = this.state;
        const onChange = page => {
            this.setState({
                currentPagination: page
            });
        };
        const pageSizeHandler = value => {
            this.setState({
                customPagesize: value
            });
        };

        const data = this.props.complains;
        data.map(d => {
            let uuid = uuidv4();
            d.key = uuid;
        })


        return (
            <div>
                <PageHeaderWrapper />
                <Card bordered={false}>
                    <ScrollTable
                        dataSource={data}
                        columns={columns}
                        title="Complain List"
                        scroll={{ x: 2250 }}
                        getData={this.getAllComplain}
                    />
                </Card>
            </div >
        );
    }
}

function mapStateToProps(state) {
    return {
        lang: state.locale.lang,
        isSignedIn: state.auth.isSignedIn,
        roleid: state.auth.roleid,
        isloaded: state.loading.isloaded,
        complains: state.complain.list,
    };
}
export default connect(
    mapStateToProps,
    { fetchComplain }
)(AssignSchedule);
