// import { Table } from 'antd';
// import React  from 'react';
// import { Link } from 'react-router-dom'
// import { DatePicker,Divider } from 'antd';
// import { Radio,Button } from 'antd';
// import { Row,Col,Form,Input } from 'antd';
// //import moment from 'moment';

// const { RangePicker } = DatePicker;
// const { TextArea } = Input;

// const columns = [
//      {
//        title: 'Name',
//        dataIndex: 'name',
//      },
//      {
//        title: 'NRIC',
//        dataIndex: 'nric',
//      },
//      {
//        title: 'Employee Code',
//        dataIndex: 'code',
//      },
//      {
//        title: 'Address',
//        dataIndex: 'parmanent_address',
//      },
//      {
//        title: 'Phone No',
//        dataIndex: 'phone',
//      },
  
//      {
//        title: 'Action',
//        render: record => (
//            <>
//               <Link style={{ color: 'green',marginRight:'0.5em' }} to={"/employees/detail/"+record.id}>View</Link>
//          </>
//        )
//      },
//   ];


// class SelectTable extends React.Component {
//   constructor(props) {
//     super(props);

//     this.state = {
//       data: props.dataSource,
//       count: props.count,
//       currentPagination: 1,
//       customPagesize: 5,
//       selectedRowKeys: [],
      
//     };
   
//   }

//   onSelectChange = selectedRowKeys => {
//     //console.log('selectedRowKeys changed: ', selectedRowKeys);
//     this.setState({ selectedRowKeys });
//   };

//   componentDidUpdate(prevProps) {
//     // Typical usage (don't forget to compare props):
//     if (this.props.dataSource !== prevProps.dataSource) {
//       this.setState({
//         loading: false,
//         data: this.props.dataSource,
//         count: this.props.count
//       });
//     }
//   }

//   render() {
    
//     const { data,selectedRowKeys } = this.state;
//     const dateFormat = 'YYYY/MM/DD';
//     const rowSelection = {
//       selectedRowKeys,
//       onChange: this.onSelectChange,
//       hideDefaultSelections: true,
//       selections: [
//         {
//           key: 'all-data',
//           text: 'Select All Data',
//           onSelect: () => {
//             this.setState({
//               selectedRowKeys: [...Array(46).keys()], // 0...45
//             });
//           },
//         },
//         {
//           key: 'odd',
//           text: 'Select Odd Row',
//           onSelect: changableRowKeys => {
//             let newSelectedRowKeys = [];
//             newSelectedRowKeys = changableRowKeys.filter((key, index) => {
//               if (index % 2 !== 0) {
//                 return false;
//               }
//               return true;
//             });
//             this.setState({ selectedRowKeys: newSelectedRowKeys });
//           },
//         },
//         {
//           key: 'even',
//           text: 'Select Even Row',
//           onSelect: changableRowKeys => {
//             let newSelectedRowKeys = [];
//             newSelectedRowKeys = changableRowKeys.filter((key, index) => {
//               if (index % 2 !== 0) {
//                 return true;
//               }
//               return false;
//             });
//             this.setState({ selectedRowKeys: newSelectedRowKeys });
//           },
//         },
//       ],
//       onSelection: this.onSelection,
//     };
//     return (
//         <div>
//           <Form onSubmit={this.handleSubmit} >
//           <Row>
//             <Col span={9} offset={1}>
//               <Form.Item label='Interval Date:'>
//                  <RangePicker
//           // defaultValue={[moment('2015/01/01', dateFormat), moment('2015/01/01', dateFormat)]}
//                   format={dateFormat}/>
//               </Form.Item>
//             </Col>
//           </Row>
//           <Row>
//             <Col span={9} offset={1}>
//               <Form.Item label='Amount:'>
//                 <Input  addonAfter="Kyats" />
//               </Form.Item>
//             </Col>
//             <Col span={9} offset={1}>
//               <Form.Item label='Service Charge:'>
//                 <Input  addonAfter="Kyats" />
//               </Form.Item>
//             </Col>
//           </Row>
//           <Row>
//             <Col span={4} offset={1}>
//               <Form.Item label='Inspection:'>
//               <Radio.Group>
//                 <Radio value='yes'>Yes</Radio>
//                 <Radio value='no'>No</Radio>
//               </Radio.Group>
//               </Form.Item>
//             </Col>
//             <Col span={4}>
//               <Form.Item label='Service Charge:'>
//               <Radio.Group>
//                 <Radio value='yes'>Yes</Radio>
//                 <Radio value='no'>No</Radio>
//               </Radio.Group>
//               </Form.Item>
//             </Col>
//           </Row>
//           <Row>
//             <Col span={9} offset={1}>
//               <Form.Item label='Job Code:'>
//                 <Input placeholder="Enter Code" />
//               </Form.Item>
//             </Col>
//             <Col span={9} offset={1}>
//               <Form.Item label='Job Status:'>
//                 <Input placeholder="Enter Code" />
//               </Form.Item>
//             </Col>
//           </Row>
//           <Row>
//             <Col span={9} offset={1}>
//               <Form.Item label='Job Title:'>
//                 <Input placeholder="Enter Title" />
//               </Form.Item>
//             </Col>
//             <Col span={9} offset={1}>
//               <Form.Item label='Job Description:'>
//                 <TextArea rows={3} placeholder='Enter Description' />
//               </Form.Item>
//             </Col>
//           </Row>
          
//           <Table 
//           key={data.key}
//           rowSelection={rowSelection}
//           dataSource={data}
//           columns={columns}
//           rowClassName="editable-row"
//           bordered
//            />

//           <Button htmlType="submit" shape='round' style={{ fontSize: '15', color: 'white', backgroundColor: '#0014C0' }}>Submit</Button>

//           <Button shape='round' style={{ marginLeft: '10px', color: 'white', backgroundColor: '#D10000' }} onClick={this.handleCancel}>Cancel</Button>
//        </Form>
//         </div>);
//   }
// }
// export default SelectTable;