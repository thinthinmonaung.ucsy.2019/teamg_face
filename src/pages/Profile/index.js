import React, { Component } from "react";
import { Input } from 'antd';
import { Button } from 'antd';//button
import { Row, Col,Card } from 'antd';
import PageHeaderWrapper from '../../components/PageHeaderWrapper'
import history from '../../router/history'
import { getUserInfo } from '../../utils'
import api from '../../apis'
import { Avatar } from 'antd';
import moment from 'moment';
const imgurl = "http://localhost:9991/"

class Form extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            data: [],
            loading: true
        }
    }

    async componentWillMount() {
        const user = getUserInfo();
        const response = await api.get('/account/user/' + user.id);
        this.setState({ data: response.data.data })
    }
    
    btnEdit = () => {
        history.push("proedit");

    }

    render() {

        const { data } = this.state;
        console.log(data);
        return (
            <div>
                <PageHeaderWrapper />
                <Card style={{margin:'3%'}}>
                <Col span={24}>
                    <Avatar
                        style={{ marginLeft: '36%', border: '1px solid gray' }}
                        src={imgurl + data.empimage}
                        shape="circle" size={124}
                    />
                    <div style={{ marginLeft: '42%', marginBottom: '30px' }}>
                    </div>
                </Col>
                <div style={{margin:'9%'}}>
                    <Row>
                        <Col span={8}>
                            <h3>Name</h3>
                            <p>{data.empname}</p>
                        </Col>
                        <Col span={8}>
                            <h3>NRIC</h3>
                            <p>{data.empnric}</p>
                        </Col>
                        <Col span={8}>
                            <h3>Date Of Birth</h3>
                            <p> {moment(data.empdob).format('YYYY-MM-DD')}</p>
        </Col>
                    </Row><br />
                    <Row>
                        <Col span={8}>
                            <h3>Position</h3>
                            <p>{data.posname}</p>
        </Col>
                        <Col span={8}>
                            <h3>Department</h3>
                            <p>{data.depname}</p>
        </Col>
                        <Col span={8}>
                            <h3>Employee Code</h3>
                            <p>{data.ecode}</p>
        </Col>
                    </Row><br />
                    <Row>
                        <Col span={8}>
                            <h3>Father</h3>
                            <p>{data.empfather}</p>
        </Col>
                        <Col span={8}>
                            <h3>Mother</h3>
                            <p>{data.empmother}</p>
        </Col>
                        <Col span={8}>
                            <h3>Phone No</h3>
                            <p>{data.empprefix}{data.phone_number}</p>
        </Col>
                    </Row><br />
                    <Row>
                        <Col span={8}>
                            <h3>Parment</h3>
                            <p>{data.empparmanent}</p>
        </Col>
                        <Col span={8}>
                            <h3>Tamporery</h3>
                            <p>{data.emptemporary}</p>
        </Col>

                    </Row><br />
                    <Row>
                        <Col span={8}>
                            <h3>Education</h3>
                            <p>{data.empedu}</p>
        </Col>
                        <Col span={8}>
                            <h3>Social Media</h3>
                            <p>{data.empsocial}</p>
        </Col>

                    </Row><br />
                </div>
                </Card>
            </div>
        )
    }
}
export default Form