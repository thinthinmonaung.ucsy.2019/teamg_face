import React from 'react'
import { Col,Form,Button,Breadcrumb,Card }from 'antd'
import history from '../../router/history'
import api from '../../apis';

class View extends React.Component{

    constructor(props){
        super(props);
        this.state = {
            code : this.props.match.params.id,
            data : null
        }
    }

    componentDidMount() {
        this.getData();
    }

    async getData() {
        const response = await api.get(`report/${this.state.code}`);
        if(response && response.status == 200){
            let data = response.data.data;
            this.setState({data: data})
        }
    }

    btnBack=()=>{
    history.push('/report/')
    }

   
    render(){

        const { data } = this.state;
        if(!data){
            return <div>Data is waiting!!!!</div>      //data waiting meassage
        }
        console.log(data);

        return(
            <Form style={{marginLeft:'10px'}}>
                <Breadcrumb style={{fontSize:'13px',fontWeight:'bold',marginBottom:'20px'}}>
                    <Breadcrumb.Item>
                    <a href="/">Dashboard</a>
                    </Breadcrumb.Item>
                    <Breadcrumb.Item>
                    <a href="/report">Daily Report</a>
                    </Breadcrumb.Item>
                </Breadcrumb>
                   <Card title='Daily Report'>
                    <div style={{marginLeft:'10%',fontSize:'13px'}}>
                    <div>
                    <Col span={12}>
                            <h3>Job Code:</h3>
                            <p style={{fontSize:'13px'}}>{data.job_code}</p>
                        </Col>
                        <Col span={12}>
                            <h3>Date:</h3>
                            <p style={{fontSize:'13px'}}>{data.date}</p>
                        </Col>
                        
                        
                    </div><br></br><br></br><br></br><br></br>
                   
                    <div>
                        <Col span={12}>
                            <h3>FUP No:</h3>
                            <p style={{fontSize:'13px'}}>{data.fup_number}</p>
                        </Col>
                        <Col span={12}>
                            <h3>Working Hour:</h3>
                            <p style={{fontSize:'13px'}}>{data.wkh}</p> 
                        </Col>
                        
                    </div><br></br><br></br><br></br><br></br>
                    <div>
                        <Col span={12} style={{marginBottom:'80px'}}>
                            <h3>Warranty Description:</h3>
                            <p style={{fontSize:'13px'}}>{data.wd}</p>
                        </Col>
                        <Col span={12}  style={{align:'center'}}>
                            <h3>Remark:</h3>
                            <p style={{fontSize:'13px'}}>{data.remark}</p>
                        </Col>
                    </div>

                    <div>
                        <Form.Item wrapperCol={{ span: 24 }}>
                           
                        <Button type="primary"  style={{backgroundColor:"green",color:"white",borderColor:'green',padding:'0px 27px',marginLeft:'30%'}} onClick={this.btnBack}>
                            Back
                            </Button>
                        </Form.Item>
                    </div>
                </div>
                </Card>
            </Form>
        );
    }
}

export default View;