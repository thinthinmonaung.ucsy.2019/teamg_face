import React from 'react'

import {
  Form,
  Breadcrumb,
  Button, Row, Col,
  Input, Select, DatePicker
} from 'antd';
import { connect } from "react-redux";
import history from '../../router/history'
import api from '../../apis';
import { noti } from '../../utils/index';
import PageHeaderWrapper from '../../components/PageHeaderWrapper';

import { fetchComplain, postComplain } from '../../actions/Complain';
import { fetchSchedules } from '../../actions/Schedule';
import { fetchMachines } from '../../actions/Machine';

const { Option } = Select;
class CreateComplain extends React.Component {
  constructor(props) {
    super(props)

  }

  componentDidMount() {
    this.props.fetchSchedules();
    this.props.fetchMachines();
  }

  state = {
    size: 'large',
  };

  handleSubmit = e => {
    e.preventDefault();
    this.props.form.validateFieldsAndScroll((err, fieldsValue) => {
      if (!err) {
        const values = {
          ...fieldsValue,
          'date': fieldsValue['date'].format('YYYY-MM-DD'),
        }

        console.log(values);

        api.post('report', values).then((result) => {

          console.log("Result", result);
          this.props.history.push('/report')
          noti('success', 'Successfully!', 'Report has been created successfully.')

        })
      } else {
        noti('error', 'Unsuccessfully!', 'Fail to Create.')
      }
    });
  };

  btnCancel = () => {
    history.push('/report')
  }

  render() {
    const size = this.state.size;

    const { getFieldDecorator } = this.props.form;

    const renderSchedule = (
      <Select style={{
        width: '300px',
        marginleft: '10px',
        display: 'inline-block'
      }} placeholder="Please select Schedule">
        {this.props.schedule.map(item => {
          return <Option value={item.job_code} >{item.job_code}</Option>
        })}
      </Select>
    )

    const renderMachine = (
      <Select style={{
        width: '300px',
        marginleft: '10px',
        display: 'inline-block'
      }} placeholder="Please select machine">
        {this.props.machine.map(item => {
          return <Option value={item.fup_number}>{item.fup_number}</Option>
        })}
      </Select>
    )
    return (
      <div>
        <PageHeaderWrapper />
        <div style={{ marginLeft: '0.1%' }}>
          <h2 style={{ marginBottom: '2.5%' }}>Create Daily Report</h2>
          <br />

          <Form onSubmit={this.handleSubmit} style={{ border: '1px solid gray', borderRadius: '5px' }}>

            <div style={{ marginLeft: '8%', marginTop: '5%' }}>
              <Row>
                <Col span={10}>
                  <Form.Item style={{
                    width: '80%',
                  }} label="Job Code:">
                    {getFieldDecorator('job_code', {
                      rules: [{
                        required: true,
                        message: 'Please input your Job Code!'
                      }]
                    })(
                      renderSchedule
                    )}
                  </Form.Item>
                </Col>
                <Col span={14}>
                  <Form.Item style={{
                    width: '70%',
                  }} label="Date:">
                    {getFieldDecorator('date', {
                      rules: [{
                        required: true,
                        message: 'Please input date!'
                      }]
                    })(
                      <DatePicker style={{
                        width: '300px',
                      }} placeholder="yy/mm/dd" />
                    )}
                  </Form.Item>
                </Col>
              </Row>
              <Row>
                <Col span={10}>
                  <Form.Item style={{
                    width: '70%',
                  }} label="FUP NO:">
                    {
                      getFieldDecorator('fup_number', {
                        rules: [{
                          required: true,
                          message: 'Please input FUP number!'
                        }]
                      })(
                        renderMachine

                      )}
                  </Form.Item>
                </Col>
                <Col span={14}>
                  <Form.Item style={{
                    width: '70%',
                  }} label="Working Hour:">
                    {getFieldDecorator('wkh', {
                      rules: [{
                        required: true,
                        message: 'Please input Working Hour!'
                      }]
                    })(<Input placeholder="Enter Working Hour" />)}
                  </Form.Item>
                </Col>
              </Row>
              <Row>
                <Col span={10}>

                  <Form.Item style={{
                    width: '80%',
                  }} label="Warranty Description">
                    {getFieldDecorator('wd', {
                      rules: [{
                        required: true,
                        message: 'Please input your Warranty Description !'
                      }]
                    })(<Input style={{
                      marginleft: '10px',
                      height: '80px'
                    }} placeholder="Enter Warranty Description" />)}
                  </Form.Item>
                </Col>
                <Col span={14}>
                  <Form.Item style={{
                    width: '70%',
                  }} label="Remark:">
                    {getFieldDecorator('remark', {
                      rules: [{
                        required: true,
                        message: 'Please input Remark!'
                      }]
                    })(<Input style={{
                      marginleft: '10px',
                      height: '80px'
                    }} placeholder="Enter Remark" />)}
                  </Form.Item>
                </Col>
              </Row>
            </div>
            <br />
            <div style={{ marginLeft: '35%', marginBottom: '5%' }}>
              <Button htmlType="submit" style={{ color: 'white', backgroundColor: 'green', borderColor: 'green' }}> Report</Button>
              <Button onClick={this.btnCancel} style={{ marginLeft: '15px', color: 'black', backgroundColor: 'white', borderColor: 'gray' }}> Cancel</Button>

            </div>
          </Form>
        </div>
      </div>



    );
  }
}


const Complain = Form.create()(CreateComplain);

function mapStateToProps(state) {
  return {
    lang: state.locale.lang,
    isSignedICreateEmployeen: state.auth.isSignedIn,
    roleid: state.auth.roleid,
    isloaded: state.loading.isloaded,
    schedule: state.schedule.list,
    machine: state.machine.list
  };
}

export default connect(
  mapStateToProps,
  { postComplain, fetchComplain, fetchSchedules, fetchMachines }
)(Complain);