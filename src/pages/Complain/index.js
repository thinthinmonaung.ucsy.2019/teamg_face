import React from 'react';
import { Table, Divider, Tag } from 'antd';
import { Breadcrumb,Icon } from 'antd';
import { Button } from 'antd';
import { Input, Popconfirm } from 'antd';
import ScrollTable from '../ScrollTable/CustomScrollTable';
import { Link, Route } from 'react-router-dom'
import { fetchComplain, putComplain, postComplain, deleteComplain } from '../../actions/Complain'
import { connect } from "react-redux";
import './index.css';
import PageHeaderWrapper from '../../components/PageHeaderWrapper'
// import {  Typography } from 'antd';

const uuidv4 = require('uuid/v4');

const Search = Input.Search;
// const { Paragraph } = Typography;



const routes = [
  {
    path: 'Configuration',
    breadcrumbName: 'Configuration'
  },

  {
    path: 'Complains',
    breadcrumbName: 'Complains'
  }

];

class Complain extends React.Component {

  constructor(props) {
    super(props);
    this.state = {

    }
  }


  componentDidMount() {
    this.getAllComplain();
  }

  getAllComplain() {
    console.log(this.props.fetchComplain())
  }

  delete = key => {
    const newData = this.props.complains;
    const index = newData.findIndex(item => key === item.key);
    const item = newData[index];

    this.deleteCpm(item.id);
    // message.success('Deleted !');
  };

  deleteCpm = (id) => {
    this.props.deleteComplain(id);
  }




  render() {
    const columns = [
      {
        title: 'Customer Name',
        dataIndex: 'customer_name',
        key:'customer_name',
        sorter: (a, b) => a.customer_name.length - b.customer_name.length,
        sortDirections: ['ascend', 'descend'],
        fixed: 'left',
        width: 200,
        align:'center',
      },

      {
        title: 'Complain No',
        dataIndex: 'complain_number',
        key:'1',
        align:'center',
        sorter: (a, b) => a.complain_number.length - b.complain_number.length,
        sortDirections: ['ascend', 'descend'],
      },
      {
        title: 'Model No',
        dataIndex: 'model_number',
        key:'2',
        align:'center',
        sorter: (a, b) => a.model_number.length - b.model_number.length,
        sortDirections: ['ascend', 'descend'],
      },
      {
        title: 'Distance',
        dataIndex: 'distance',
        key:'3',
        align:'center',
      },
      {
        title: 'Location',
        dataIndex: 'location',
        key:'4',
        align:'center',
      },

      {
        title: 'Action',
        key: 'operation',
        width: 150,
        render: (text, record) => (
          <span>
            <Link
              to={"/complains/detail/" + record.id}
              style={{ color: 'green' }}>
                <Icon type="eye" style={{ color: '#00FF00' }} />
            </Link>
            <Divider type="vertical" />

            <Link to={"complains/edit/" + record.id} style={{ color: 'blue' }} >
            <Icon type="edit" />
            </Link>
            <Divider type="vertical" />
            <Popconfirm
              onConfirm={() => this.delete(record.key)}
              onCancel={this.cancel}
              placement="left"
              title="Are you sure delete?"
              okText="Yes"
              cancelText="Cancel"
              okType="danger"
              style={{ backgroundColor: '#33ff33' }}
            >
              <a style={{ color: '#ff3333' }}><Icon type="delete" /></a>
            </Popconfirm>
          </span>
        ),
        fixed: 'right',
        align:'center',

      }
    ];
    let dataSource = this.props.complains;
    console.log("DataSouce", dataSource);

    dataSource.map(d => {
      let uuid = uuidv4();
      d.key = uuid;
    })

    return (
      <div className='wrap'>
        <PageHeaderWrapper />
        <Link to="/complains/create">
          <Button style={{ color: 'white', backgroundColor: '#395D42', marginBottom: '30px', height: '50px' }} size="medium">Create New Complain</Button>
        </Link>

        <div>
          <ScrollTable
            dataSource={dataSource}
            columns={columns}
            title="Complain List"

          />
        </div>
      </div>

    );
  }
}

function mapStateToProps(state) {
  return {
    lang: state.locale.lang,
    isSignedIn: state.auth.isSignedIn,
    roleid: state.auth.roleid,
    isloaded: state.loading.isloaded,
    complains: state.complain.list,
  };
}
export default connect(
  mapStateToProps,
  { fetchComplain, putComplain, postComplain, deleteComplain }
)(Complain);