import React from 'react'

import {
    Form,
    Breadcrumb,
    Button, Icon,
    Upload, message, Divider, Row, Col,
    Input, Select, DatePicker
} from 'antd';
import { connect } from "react-redux";
import history from '../../../router/history'
import api from 'apis';
import { noti } from 'utils/index';


import { fetchComplain, postComplain } from '../../../actions/Complain';
import { fetchDepartment } from '../../../actions/Department';
import { fetchMachines } from '../../../actions/Machine';

import { Link } from 'react-router-dom'
const moment = require('moment');







const { Option } = Select;
class CreateComplain extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            id: this.props.match.params.id,
            data: [],

        };

    }

    componentDidMount() {
        this.props.fetchDepartment();
        this.props.fetchMachines();
        this.getData()
        this.getAllComplain();
    }

    async  getData() {
        const response = await api.get(`complains/${this.state.id}`);
        // this.props.fetchPosition();
        if (response && response.status == 200) {
          let data = response.data.data;
    
          this.setState({ data: data })
          this.setInitialValues();
        }
      }

      setInitialValues = () => {
        const data = this.state.data;
        const { form } = this.props;
        if (data)
          form.setFieldsValue({
            complain_number: data.complain_number,
            machine_id: data.machine_id,
            department_id: data.department_id,
            job_title: data.job_title,
            complain_job_title: data.complain_job_title,
            complain_description: data.complain_description,
            amount: data.amount,
            customer_name: data.customer_name,
            customer_phone: data.customer_phone,
            location: data.location,
            distance: data.distance,
            date: moment(data.date),
            



          });
      };

    getAllComplain() {
        this.props.fetchComplain();
    }

    state = {
        size: 'large',
    };




    handleSubmit = e => {
        e.preventDefault();
        this.props.form.validateFieldsAndScroll((err, fieldsValue) => {
            if (!err) {
                const values = {
                    ...fieldsValue,
                    'date': fieldsValue['date'].format('YYYY-MM-DD'),
                }

                console.log(values);
                api.put(`complains/${this.state.id}`, values).then((result) =>{
                    console.log(result)
                    this.props.history.push('/complains')
                  noti('success', 'Successfully!', 'Complain has been updated successfully.')
                  })
            } else {
                noti('error', 'Unsuccessfully!', 'Fail to Create.')
            }
        });
    };

    btnCancel = () => {
        history.push('/complains')
      }


    render() {


        const size = this.state.size;

        const { getFieldDecorator } = this.props.form;

        const prefixSelector = getFieldDecorator('prefix', {
            initialValue: '+95',
        })(
            <Select style={{ width: 60 }}>
                <Option value="+95">+95</Option>
                <Option value="+78">+78</Option>
                <Option value="+96">+96</Option>

            </Select>,
        );
        const renderDepartment = (
            <Select style={{
                width: '300px',
                marginleft: '10px',
                display: 'inline-block'
            }} placeholder="Please select department">
                {this.props.department.map(item => {
                    return <Option value={item.id}>{item.name}</Option>
                })}
            </Select>
        )

        const renderMachine = (
            <Select style={{
                width: '300px',
                marginleft: '10px',
                display: 'inline-block'
            }} placeholder="Please select machine">
                {this.props.machine.map(item => {
                    return <Option value={item.id}>{item.modelnumber}</Option>
                })}
            </Select>
        )

        return (
            <div>
             <Breadcrumb style={{ fontSize: '13px', fontWeight: 'bold', marginBottom: '5%' }}>
                <Breadcrumb.Item>
                    <a href="/">Configuration</a>
                </Breadcrumb.Item>
                <Breadcrumb.Item>
                    <a href="/complains">Complain</a>
                </Breadcrumb.Item>
                <Breadcrumb.Item>
                    <a href="" style={{ color: 'green' }}>Edit Complain</a>
                </Breadcrumb.Item>
            </Breadcrumb>
                <Form onSubmit={this.handleSubmit} >
                    <div style={{marginLeft:'2.5%'}}>
                        <Row>
                            <Col span={8}>
                            <Form.Item style={{
                                width: '60%'
                            }} label="Complain Number:">
                                {getFieldDecorator('complain_number', {
                                    rules: [{
                                        required: true,
                                        message: 'Please input your comaplain number'
                                    }]
                                })(<Input placeholder="Enter number" />)}
                            </Form.Item>
                            </Col>
                            <Col span={9}>
                            <Form.Item style={{
                                width: '80%',
                            }} label="Complain Job Title:">
                                {getFieldDecorator('complain_job_title', {
                                    rules: [{
                                        required: true,
                                        message: 'Please input your complain job title!'
                                    }]
                                })(<Input placeholder="Enter complain title" />)}
                            </Form.Item>
                                </Col>
                                <Col span={7}>
                                <Form.Item style={{
                                width: '60%'
                            }} label="Job Title:">
                                {getFieldDecorator('job_title', {
                                    rules: [{
                                        required: true,
                                        message: 'Please input your Job Title!'
                                    }]
                                })(
                                    <Input placeholder="Enter title" />
                                )}
                            </Form.Item>
                                </Col>
                        </Row>

                        <Row>
                            <Col span={8}>

                            <Form.Item style={{
                                width: '60%',
                            }} label="Description:">
                                {
                                    getFieldDecorator('complain_description', {
                                        rules: [{
                                            required: true,
                                            message: 'Please input description!'
                                        }]
                                    })(
                                        <Input placeholder="Enter Description" />

                                    )}
                            </Form.Item>
                            </Col>
                            <Col span={16}>
                            <Form.Item style={{
                                width: '60%',
                            }} label="Department:">
                                {getFieldDecorator('department_id', {
                                    rules: [{
                                        required: true,
                                        message: 'Please input your Department!'
                                    }]
                                })(
                                    renderDepartment
                                )}
                            </Form.Item>
                                </Col>
                        </Row>

                        <Row>
                            <Col span={8}>
                            <Form.Item style={{
                                width: '60%',
                            }} label="Amount:">
                                {getFieldDecorator('amount', {
                                    rules: [{
                                        required: true,
                                        message: 'Please input your amount!'
                                    }]
                                })(<Input placeholder="Enter amount" />)}
                            </Form.Item>
                            </Col>
                            <Col span={16}>
                            <Form.Item style={{
                            width: '60%',
                        }} label="Model number:">
                            {
                                getFieldDecorator('machine_id', {
                                    rules: [{
                                        required: true,
                                        message: 'Please input model number!'
                                    }]
                                })(
                                    renderMachine

                                )}
                        </Form.Item>
                                </Col>
                        </Row>

                        <Row>
                            <Col span={8}>
                            <Form.Item style={{
                                width: '60%',
                            }} label="Customer Name:">
                                {getFieldDecorator('customer_name', {
                                    rules: [{
                                        required: true,
                                        message: 'Please input name!'
                                    }]
                                })(<Input placeholder="Enter name" />)}
                            </Form.Item>
                            </Col>
                                <Col span={16}>
                                
                            <Form.Item style={{
                                width: '60%',
                            }} label="Date:">
                                {getFieldDecorator('date', {
                                    rules: [{
                                        required: true,
                                        message: 'Please input date!'
                                    }]
                                })(
                                    <DatePicker style={{
                                        width: '300px',


                                    }} placeholder="yy/mm/dd" />
                                )}
                            </Form.Item>
                                </Col>
                        </Row>
                        <Row>
                        <Col span={8}>
                            <Form.Item style={{
                                width: '60%'
                            }} label="Distance">
                                {getFieldDecorator('distance', {
                                    rules: [{
                                        required: true,
                                        message: 'Please input your Distance !'
                                    }]
                                })(<Input placeholder="Enter Distance" />)}
                            </Form.Item>
                                </Col>
                                <Col span={9}>
                                <Form.Item style={{
                                width: '85%',
                            }} label="Customer Ph No.:">
                                {getFieldDecorator('customer_phone', {
                                    rules: [{
                                        required: true,
                                        message: 'Please input your phone no:!'
                                    }]
                                })(<Input  addonBefore={prefixSelector} placeholder="0 0000 0000" />)}
                            </Form.Item>
                                </Col>
                                <Col span={7}>
                                <Form.Item style={{
                                width: '60%',
                            }} label="Location:">
                                {getFieldDecorator('location', {
                                    rules: [{
                                        required: true,
                                        message: 'Please input Location!'
                                    }]
                                })(<Input placeholder="Enter location" />)}
                            </Form.Item>
                                    </Col>
                            </Row>
                    </div>
                    <Button type="primary" htmlType="submit" style={{ marginLeft: '2.5%', backgroundColor: "green", color: "white", borderColor: "green" }}>
                Update
                        </Button>

              <Button type="primary" style={{ marginLeft: '2%', backgroundColor: "white", color: "black", borderColor: "gray" }} onClick={this.btnCancel} >
                Cancel
                    </Button>
                </Form>
            </div>



        );
    }
}


const Complain = Form.create()(CreateComplain);

function mapStateToProps(state) {
    return {
        lang: state.locale.lang,
        isSignedICreateEmployeen: state.auth.isSignedIn,
        roleid: state.auth.roleid,
        isloaded: state.loading.isloaded,
        department: state.department.list,
        machine: state.machine.list
    };
}

export default connect(
    mapStateToProps,
    { postComplain, fetchComplain, fetchDepartment, fetchMachines }
)(Complain);