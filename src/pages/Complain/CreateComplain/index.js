import React from 'react'

import {
    Form,
    Breadcrumb,
    Button, Icon,
    Upload, message, Divider, Row, Col,
    Input, Select, DatePicker
} from 'antd';
import { connect } from "react-redux";
import history from '../../../router/history'
import api from 'apis';
import { noti } from 'utils/index';
import PageHeaderWrapper from '../../../components/PageHeaderWrapper';

import { fetchComplain, postComplain } from '../../../actions/Complain';
import { fetchDepartment } from '../../../actions/Department';
import { fetchMachines } from '../../../actions/Machine';

import { Link } from 'react-router-dom'







const { Option } = Select;
class CreateComplain extends React.Component {
    constructor(props) {
        super(props)

    }

    componentDidMount() {
        this.props.fetchDepartment();
        this.props.fetchMachines();

        this.getAllComplain();
    }

    getAllComplain() {
        this.props.fetchComplain();
    }

    state = {
        size: 'large',
    };






    handleSubmit = e => {
        e.preventDefault();
        this.props.form.validateFieldsAndScroll((err, fieldsValue) => {
            if (!err) {
                const values = {
                    ...fieldsValue,
                    'date': fieldsValue['date'].format('YYYY-MM-DD'),
                }

                console.log(values);

                api.post('complains', values).then((result) => {

                    console.log("Result", result);
                    this.props.history.push('/complains')
                    noti('success', 'Successfully!', 'Complain has been created successfully.')

                })
            } else {
                noti('error', 'Unsuccessfully!', 'Fail to Create.')
            }
        });
    };

    btnCancel=()=>{
        history.push('/complains')
    }

    render() {
        const size = this.state.size;

        const { getFieldDecorator } = this.props.form;

        const prefixSelector = getFieldDecorator('prefix', {
            initialValue: '+95',
        })(
            <Select style={{ width: 70 }}>
                <Option value="+95">+95</Option>
                <Option value="+87">+87</Option>
                <Option value="+96">+96</Option>

            </Select>,
        );
        const renderDepartment = (
            <Select style={{
                width: '300px',
                marginleft: '10px',
                display: 'inline-block'
            }} placeholder="Please select department">
                {this.props.department.map(item => {
                    return <Option value={item.id}>{item.name}</Option>
                })}
            </Select>
        )

        const renderMachine = (
            <Select style={{
                width: '300px',
                marginleft: '10px',
                display: 'inline-block'
            }} placeholder="Please select machine">
                {this.props.machine.map(item => {
                    return <Option value={item.id}>{item.modelnumber}</Option>
                })}
            </Select>
        )
        return (
            <div>
                <PageHeaderWrapper />
                <div style={{ marginLeft: '5%' }}>
                    <h2 style={{ marginBottom: '2.5%' }}>Create Complain</h2>
                    <p> You can add Complain basic data by entering one by one using the following form.</p>
                    <br />

                    <Form onSubmit={this.handleSubmit} >

                        <div>
                            <Row>
                                <Col span={10}>
                                <Form.Item style={{
                                    width: '80%',
                                }} label="Complain Number:">
                                    {getFieldDecorator('complain_number', {
                                        rules: [{
                                            required: true,
                                            message: 'Please input your comaplain number'
                                        }]
                                    })(<Input placeholder="Enter number" />)}
                                </Form.Item>
                                    </Col>

                                    <Col span={14}>
                                <Form.Item style={{
                                    width: '70%',
                                }} label="Complain Job Title:">
                                    {getFieldDecorator('complain_job_title', {
                                        rules: [{
                                            required: true,
                                            message: 'Please input your complain job title!'
                                        }]
                                    })(<Input placeholder="Enter complain title" />)}
                                </Form.Item>
                                        </Col>
                                </Row>

                                <Row>
                                    <Col span={10}>
                                    <Form.Item style={{
                                    width: '80%',
                                }} label="Job Title:">
                                    {getFieldDecorator('job_title', {
                                        rules: [{
                                            required: true,
                                            message: 'Please input your Job Title!'
                                        }]
                                    })(
                                        <Input  placeholder="Enter title" />
                                    )}
                                </Form.Item>
                                        </Col>
                                        <Col span={14}>
                                        <Form.Item style={{
                                    width: '70%',
                                }} label="Description:">
                                    {
                                        getFieldDecorator('complain_description', {
                                            rules: [{
                                                required: true,
                                                message: 'Please input description!'
                                            }]
                                        })(
                                            <Input style={{
                                                marginleft: '10px',
                                                height: '70px',
                                            }} placeholder="Enter Description" />

                                        )}
                                </Form.Item>
                                        </Col>
                                </Row>
                                <Row>
                                    <Col span={10}>
                                    <Form.Item style={{
                                    width: '80%',
                                }} label="Department:">
                                    {getFieldDecorator('department_id', {
                                        rules: [{
                                            required: true,
                                            message: 'Please input your Department!'
                                        }]
                                    })(
                                        renderDepartment
                                    )}
                                </Form.Item>
                                        </Col>
                                        <Col span={14}>
                                        <Form.Item style={{
                                width: '70%',
                            }} label="Model number:">
                                {
                                    getFieldDecorator('machine_id', {
                                        rules: [{
                                            required: true,
                                            message: 'Please input model number!'
                                        }]
                                    })(
                                        renderMachine

                                    )}
                            </Form.Item>
                                        </Col>
                                </Row>
                                <Row>
                                    <Col span={10}>
                                    <Form.Item style={{
                                    width: '80%',
                                }} label="Amount:">
                                    {getFieldDecorator('amount', {
                                        rules: [{
                                            required: true,
                                            message: 'Please input your amount!'
                                        }]
                                    })(<Input placeholder="Enter amount" />)}
                                </Form.Item>
                                        </Col>
                                        <Col span={14}>
                                        <Form.Item style={{
                                    width: '70%',
                                }} label="Customer Name:">
                                    {getFieldDecorator('customer_name', {
                                        rules: [{
                                            required: true,
                                            message: 'Please input name!'
                                        }]
                                    })(<Input placeholder="Enter name" />)}
                                </Form.Item>
                                        </Col>
                                </Row>
                                <Row>
                                    <Col span={10}>

                                <Form.Item style={{
                                    width: '80%',
                                }} label="Distance">
                                    {getFieldDecorator('distance', {
                                        rules: [{
                                            required: true,
                                            message: 'Please input your Distance !'
                                        }]
                                    })(<Input placeholder="Enter Distance" />)}
                                </Form.Item>
                                        </Col>
                                        <Col span={14}>
                                        <Form.Item style={{
                                    width: '70%',
                                }} label="Date:">
                                    {getFieldDecorator('date', {
                                        rules: [{
                                            required: true,
                                            message: 'Please input date!'
                                        }]
                                    })(
                                        <DatePicker style={{
                                            width: '300px',
                                        }} placeholder="yy/mm/dd" />
                                    )}
                                </Form.Item>
                                        </Col>
                                </Row>
                                <Row>
                                    <Col span={10}>
                                    <Form.Item style={{
                                    width: '80%',
                                }} label="Customer Ph No.:">
                                    {getFieldDecorator('customer_phone', {
                                        rules: [{
                                            required: true,
                                            message: 'Please input your phone no:!'
                                        }]
                                    })(<Input  addonBefore={prefixSelector} placeholder="0 0000 0000" />)}
                                </Form.Item>
                                        </Col>
                                        <Col span={14}>
                                        <Form.Item style={{
                                    width: '70%',
                                }} label="Location:">
                                    {getFieldDecorator('location', {
                                        rules: [{
                                            required: true,
                                            message: 'Please input Location!'
                                        }]
                                    })(<Input placeholder="Enter location" />)}
                                </Form.Item>
                                        </Col>
                                </Row>
                        </div>
                        <br/>
                        <Button htmlType="submit" style={{ color: 'white', backgroundColor: 'green', borderColor: 'green' }}> Submit</Button>
                                <Button onClick={this.btnCancel} style={{ marginLeft: '15px', color: 'black', backgroundColor: 'white', borderColor: 'gray' }}> Cancel</Button>
                            
                    </Form>
                </div>
            </div>



        );
    }
}


const Complain = Form.create()(CreateComplain);

function mapStateToProps(state) {
    return {
        lang: state.locale.lang,
        isSignedICreateEmployeen: state.auth.isSignedIn,
        roleid: state.auth.roleid,
        isloaded: state.loading.isloaded,
        department: state.department.list,
        machine: state.machine.list
    };
}

export default connect(
    mapStateToProps,
    { postComplain, fetchComplain, fetchDepartment, fetchMachines }
)(Complain);