import React from 'react'
import { Col,Form,Avatar,Button,Breadcrumb,Row }from 'antd'
import history from '../../../router/history'
import api from 'apis';

class View extends React.Component{

    constructor(props){
        super(props);
        this.state = {
            code : this.props.match.params.id,
            data : null
        }
    }

    componentDidMount() {
        this.getData();
    }

    async getData() {
        const response = await api.get(`module/${this.state.code}`);
        if(response && response.status == 200){
            let data = response.data.data;
            this.setState({data: data})
        }
    }

    btnCancel=()=>{
    history.push('/module/')
    }

    btnEdit=()=>{
        history.push(`/module/${this.state.code}`)
        }

    render(){

        const { data } = this.state;
        if(!data){
            return <div>Data is waiting!!!!</div>      //data waiting meassage
        }
        console.log(data);

        return(
            <Form>
                <Breadcrumb style={{fontSize:'13px',fontWeight:'bold',marginBottom:'20px'}}>
                    <Breadcrumb.Item>
                    <a href="/">Dashboard</a>
                    </Breadcrumb.Item>
                    <Breadcrumb.Item>
                    <a href="/module">Module</a>
                    </Breadcrumb.Item>
                    <Breadcrumb.Item>
                    <a href="#" style={{color:'green'}}>Module View </a>
                    </Breadcrumb.Item>
                </Breadcrumb>

                <Col span={24}>
                       <div style={{marginLeft:'10%',marginBottom:'50px'}}>
                           <h2>Module Information</h2>
                        </div>
                    
                </Col>


               
                    
                <div style={{marginLeft:'10%'}}>
                    <div>
                    <Col span={9} >
                            <h3>Module Name</h3>
                            <p>{data.mname}</p>
                        </Col>
                        <Col span={14}>
                            <h3>Controller Name</h3>
                            <p>{data.cname}</p>
                        </Col>
                        
                        
                    </div><br></br><br></br><br></br><br></br><br></br><br></br><br></br>
                    <div>
                        <Col span={9} >
                            <h3>Action Name:</h3>
                            <p>{data.aname}</p>
                        </Col>
                        <Col span={12.5}>
                            <h3>Remark:</h3>
                            <p>{data.remark}</p> 
                        </Col>
                        
                    </div>
                   

                    
                   <Col style={{marginBottom:'40px'}}></Col>
                    <div>
                        <Form.Item wrapperCol={{ span: 24 }}>
                            <Button type="primary" onClick={this.btnEdit} style={{backgroundColor:"green",color:"white",borderColor:'green',padding:'0px 27px'}} onClick={this.btnEdit}>
                           Edit
                            </Button>

                            <Button type="primary" onClick={this.btnCancel} style={{backgroundColor:"white",color:"black",marginLeft:"15px",borderColor:'gray'}}>
                            Cancel
                            </Button>
                        </Form.Item>
                    </div>
                </div>
            </Form>
        );
    }
}

export default View;