import React from 'react'
import { Col, Form, Avatar, Button, Input, Row, Breadcrumb } from 'antd'
import history from '../../../router/history'
import { fetchModule, putModule, postModule, deleteModule } from '../../../actions/Module'
import { connect } from "react-redux"
import api from 'apis';
import moment from 'moment'


class Vedit extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            code: this.props.match.params.id,
            data: null
        }
    }

    componentDidMount() {
        this.getData();
    }

    async getData() {
        const response = await api.get(`module/${this.state.code}`);
        if (response && response.status == 200) {
            let data = response.data.data;
            this.setState({ data: data })
        }
    }

    handleChange = (e) => {
        this.setState({ data: { ...this.state.data, [e.target.name]: e.target.value } })
    }

    handleSubmit = () => {
        let { data } = this.state;
        data.updated_at = moment(data.updated_at).format('YYYY-MM-DD h:mm:ss')
        data.created_at = moment(data.created_at).format('YYYY-MM-DD h:mm:ss')
        
        // api.put(`machine/${data.id}`, data);
        this.props.putModule(data, data.id);
        this.props.history.push('/module')
    }

    btnCancel = () => {
        history.push('/module/')
    }


    render() {
        const { data } = this.state;
        if (!data) {
            return <div> </div>      //data waiting meassage
        }

        const { TextArea } = Input;

        return (
            <Form  onSubmit={this.handleSubmit}>
                <Breadcrumb style={{ fontSize: '13px', fontWeight: 'bold' }}>
                    <Breadcrumb.Item>
                        <a href="/">Dashboard</a>
                    </Breadcrumb.Item>
                    <Breadcrumb.Item>
                        <a href="/module">Module</a>
                    </Breadcrumb.Item>
                    <Breadcrumb.Item>
                        <a href="#" style={{ color: 'green' }}>Module Edit </a>
                    </Breadcrumb.Item>
                </Breadcrumb>
                <div style={{marginBottom:'40px'}}></div>

                
                    <h2>Module Information</h2>
                


                <div>
                    <Row style={{ marginBottom: '80px' }}>
                        <Col span={9}>
                            <h3>Module Name:</h3>
                            <Input style={{ width: '40%' }} name="mname"
                                value={data.mname}
                                onChange={this.handleChange} />
                        </Col>

                        <Col span={14}>
                            <h3>Controller Name:</h3>
                            <Input style={{ width: '30%' }} name="cname"
                                value={data.cname}
                                onChange={this.handleChange} />
                        </Col>

                        
                    </Row>

                    <Row style={{ marginBottom: '30px' }}>
                        <Col span={9}>
                            <h3>Action Name:</h3>
                            <Input style={{ width: '40%' }} value={data.aname} />
                        </Col>
                        <Col span={12.5}>
                            <h3>Remark:</h3>
                            <TextArea rows={2} style={{ width: '40%' }} value={data.remark} onChange={this.handleChange} />
                                                            
                        </Col>
                        
                    </Row>

                    

                        
                    
                
                                
                    

            
                    <Row>
                        <Form.Item wrapperCol={{ span: 24 }}>
                            <Button type="primary" onClick={this.handleSubmit} style={{ backgroundColor: "green", color: "white", borderColor: 'green' }}>
                                Update
                            </Button>
                            <Button type="primary" onClick={this.btnCancel} style={{ backgroundColor: "white", color: "black", marginLeft: "15px", borderColor: 'gray' }}>
                                Cancel
                            </Button>
                        </Form.Item>
                    </Row>
                </div>
            </Form>
        );
    }
}

function mapStateToProps(state) {
    return {
        lang: state.locale.lang,
        isSignedIn: state.auth.isSignedIn,
        roleid: state.auth.roleid,
        isloaded: state.loading.isloaded,
        module: state.module.list,
    };
}
export default connect(
    mapStateToProps,
    { fetchModule, putModule, postModule, deleteModule }
)(Vedit);