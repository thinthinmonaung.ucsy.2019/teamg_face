

import React from 'react';
import { Table, Divider, Tag } from 'antd';
import { Breadcrumb } from 'antd';
import { Button, Icon } from 'antd';
import { Input, Popconfirm } from 'antd';
import { Link, Route } from 'react-router-dom'
import { fetchMachines, fetchMachine, deleteMachine, postMachine } from '../../actions/Machine'
import ScrollTable from '../ScrollTable/CustomScrollTable';
import { connect } from "react-redux";
import './index.css';
import PageHeaderWrapper from '../../components/PageHeaderWrapper'
// import {  Typography } from 'antd';

const uuidv4 = require('uuid/v4');

const Search = Input.Search;
// const { Paragraph } = Typography;


class Employee extends React.Component {

  constructor(props) {
    super(props);
    this.state = {

    }
  }


  componentDidMount() {
    this.getAllMachine();
  }

  getAllMachine() {
    this.props.fetchMachines()

  }

  delete = key => {
    const newData = this.props.machines;
    const index = newData.findIndex(item => key === item.key);
    const item = newData[index];

    this.deleteMachine(item.id);
    // message.success('Deleted !');
  };

  deleteMachine = (id) => {
    this.props.deleteMachine(id);
  }




  render() {

    const columns = [
      {
        title: 'Machine Serial Number',
        dataIndex: 'machine_serial_number',
        key: 'machine_serial_number',
        align: 'center',
        width: 200,
        sorter: (a, b) => a.machine_serial_number.length - b.machine_serial_number.length,
        sortDirections: ['ascend', 'descend'],
        fixed: 'left'
      },
      {
        title: 'Model Number',
        dataIndex: 'modelnumber',
        key: '1',
        align: 'center',
        sorter: (a, b) => a.modelnumber.length - b.modelnumber.length,
        sortDirections: ['ascend', 'descend'],

      },
      {
        title: 'Engine Serial Number',
        dataIndex: 'engine_serial_number',
        key: '2',
        align: 'center',
        sorter: (a, b) => a.engine_serial_number.length - b.engine_serial_number.length,
        sortDirections: ['ascend', 'descend'],
      },
      {
        title: 'FUP No',
        dataIndex: 'fup_number',
        key: '3',
        align: 'center',
        sorter: (a, b) => a.fup_number - b.fup_number,
        sortDirections: ['ascend', 'descend'],
      },
      {
        title: 'Warranty Year',
        dataIndex: 'warranty_year',
        key: '4',
        align: 'center',
        sortDirections: ['descend', 'ascend'],
        sorter: (a, b) => a.warranty_year - b.warranty_year
      },
      {
        title: 'Working Hour',
        dataIndex: 'working_hour',
        key: '5',
        align: 'center',
        sorter: (a, b) => a.working_hour - b.working_hour,
        sortDirections: ['descend', 'ascend']
      },
      {
        title: 'Action',
        key: 'operation',
        align: 'center',
        width: 150,
        fixed: 'right',
        render: (text, record) => (
          <span>
            <Link
              to={"/machines/detail/" + record.id}
              style={{ color: 'green' }}>
              <Icon type="eye" style={{ color: '#00FF00' }} />
            </Link>
            <Divider type="vertical" />

            <Link to={"/machines/edit/" + record.id} style={{ color: 'blue' }} >
              <Icon type="edit" />
            </Link>
            <Divider type="vertical" />
            <Popconfirm
              title="Are you sure delete?"
              onConfirm={() => this.delete(record.key)}
              okType="danger"
            >
              <a style={{ color: '#ff3333' }}>
                <Icon type="delete" />
              </a>
            </Popconfirm>
          </span>
        ),
      }
    ];

    let dataSource = this.props.machines;
    dataSource.map(d => {
      let uuid = uuidv4();
      d.key = uuid;
    })

    return (
      <div className='wrap'>
        <PageHeaderWrapper />
        <Link to="/machines/create">
          <Button style={{ color: 'white', backgroundColor: '#395D42', marginBottom: '30px', height: '50px' }} size='large'>Create New Machine</Button>
        </Link>

        <div style={{ border: '1px solid #e8e8e8' }}>
          <ScrollTable
            dataSource={dataSource}
            columns={columns}
            title="Machine List"
          />
        </div>
      </div>

    );
  }
}

function mapStateToProps(state) {
  return {
    lang: state.locale.lang,
    isSignedIn: state.auth.isSignedIn,
    roleid: state.auth.roleid,
    isloaded: state.loading.isloaded,
    machines: state.machine.list,
  };
}
export default connect(
  mapStateToProps,
  { fetchMachine, fetchMachines, deleteMachine, postMachine }
)(Employee);
