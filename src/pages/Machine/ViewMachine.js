import React from 'react';
import { Card } from 'antd';
import { Button, Icon, Breadcrumb } from 'antd';
import { Link } from 'react-router-dom';
import { Row, Col } from 'antd';
import api from 'apis';
import history from '../../router/history'
class ViewMachine extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            id: this.props.match.params.id,
            data: []
        }
    }

    componentDidMount() {
        this.getData();
    }

    async  getData() {
        const response = await api.get(`machines/${this.state.id}`);
        if (response && response.status == 200) {
            let data = response.data.data;

            this.setState({ data: data })

        }
    }
    btnCancel = () => {
        history.push('/machines')
      }
    render() {
        // const {id}=this.props.location.state;
        const { modelnumber, fup_number,
            machine_serial_number,
            engine_serial_number, warranty_year, working_hour,warranty_description } = this.state.data;
        return (
            <div>
                <Breadcrumb style={{ fontSize: '13px', fontWeight: 'bold' }}>
                    <Breadcrumb.Item>
                        <a href="/">Configuration</a>
                    </Breadcrumb.Item>
                    <Breadcrumb.Item>
                        <a href="/machines">Machine</a>
                    </Breadcrumb.Item>
                    <Breadcrumb.Item>
                        <a href="" style={{ color: 'green' }}>Machine View</a>
                    </Breadcrumb.Item>
                </Breadcrumb>
                <Card title='Machine Information' style={{ width: '95%', marginRight: '5%', marginLeft: '5%', borderRadius: '14px', marginTop: '10%',border:'1px solid gray' }}>
                    <Row>
                        <Col span={6}>Model Number:</Col>
                        <Col span={4}>{modelnumber}</Col>
                        <Col span={6} offset={4}>FUP Number:</Col>
                        <Col span={4}>{fup_number}</Col>

                    </Row>
                    <br />
                    <br/>
                    <Row>
                        <Col span={6}>Machine Serial Number:</Col>
                        <Col span={4}>{machine_serial_number}</Col>
                        <Col span={6} offset={4}>Engine Serial Number:</Col>
                        <Col span={4}>{engine_serial_number}</Col>
                    </Row>
                    <br />
                    <br/>
                    <Row>
                        <Col span={6}>Warranty Year:</Col>
                        <Col span={4}>{warranty_year}</Col>
                        <Col span={6} offset={4}>Working Hour:</Col>
                        <Col span={4}>{working_hour}</Col>
                    </Row>
                    <br/>
                    <br/>
                    <Row>
                    <Col span={6}>Warranty Description:</Col>
                    <Col span={4}>{warranty_description}</Col>
                    </Row>
                    <br />
                </Card>
                <div style={{marginTop:'1em'}}>
                <Link to={"/machines/edit/" + this.state.id}><Button htmlType="submit" style={{ left: '3em', color: 'white', height: '35px', width: '80px', backgroundColor: '#395D42',marginRight:'60px' }}>Edit</Button></Link>
                <Button type="primary" style={{  backgroundColor: "white", color: "black", height:'35px',width:'80px',borderColor: "gray" }} onClick={this.btnCancel} >
                Cancel
                    </Button>
                </div>

            </div>)
    }
}
export default ViewMachine;