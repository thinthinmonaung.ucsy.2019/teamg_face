

import React from 'react';
import { Table, Divider, Tag, Icon } from 'antd';
import { Breadcrumb } from 'antd';
import { Button } from 'antd';
import { Input, Popconfirm } from 'antd';
import ScrollTable from '../ScrollTable/CustomScrollTable';
import { Link, Route } from 'react-router-dom'
import { fetchEmployee, putEmployee, postEmployee, deleteEmployee } from '../../actions/Employee'
import { connect } from "react-redux";
import './index.css';
import PageHeaderWrapper from '../../components/PageHeaderWrapper'

const uuidv4 = require('uuid/v4');

const Search = Input.Search;



const routes = [
  {
    path: 'Configuration',
    breadcrumbName: 'Configuration'
  },

  {
    path: 'Employees',
    breadcrumbName: 'Employees'
  }

];

class Employee extends React.Component {

  constructor(props) {
    super(props);
    this.state = {

    }
  }


  componentDidMount() {
    this.getAllEmployee();
  }

  getAllEmployee() {
    this.props.fetchEmployee()

  }
  // to create new Schedule
  createNewEmployee = (data) => {
    data.created_by = "admin";
    data.updated_by = '';
    this.props.postEmployee(data);
  }

  delete = key => {
    const newData = this.props.employees;
    const index = newData.findIndex(item => key === item.key);
    const item = newData[index];

    this.deleteEmp(item.id);
  };

  deleteEmp = (id) => {
    this.props.deleteEmployee(id);
  }

  render() {
    const columns = [
      {
        title: 'Name',
        dataIndex: 'name',
        key:'name',
        width: 150,
        fixed: 'left',
        align: 'center',
        sorter: (a, b) => a.name.length - b.name.length,
        sortDirections: ['descend', 'ascend'],
      },
      {
        title: 'NRC',
        dataIndex: 'nric',
        key:'nric',
        width: 150,
        align: 'center',
        fixed: 'left'
      },
      {
        title: 'Position',
        dataIndex: 'posname',
        align: 'center',
        key:'posname',
        width: 200,
      },
      {
        title: 'Department',
        dataIndex: 'depname',
          key:'1',
        align: 'center',
      },
      {
        title: 'StartDate',
        dataIndex: 'start_date',
        key:'2',
        align: 'center',
      },
      {
        title: 'Address',
        dataIndex: 'parmanent_address',
        key:'3',
        align: 'center',
      },
      {
        title: 'Code',
        dataIndex: 'code',
        key:'4',
        align: 'center',
      },
      {
        title: 'DateofBirth',
        dataIndex: 'dob',
        key:'5',
        align: 'center',
      },
      {
        title: 'Email',
        dataIndex: 'email',
        key:'6',
        align: 'center',
      },
      {
        title: 'Education',
        dataIndex: 'education',
        key:'7',
        align: 'center',
      },
      {
        title: 'Social Media Link',
        dataIndex: 'social_media_link',
        key:'8',
        align: 'center',
      },
      {
        title: 'Father Name',
        dataIndex: 'father_name',
        key:'9',
        align: 'center',
      },
      {
        title: 'Mother Name',
        dataIndex: 'mother_name',
        key:'10',
        align: 'center',
      },
      {
        title: 'Action',
        key: 'operation',
        align:'center',
        width: 150,
        render: (text, record) => (
          <span>
            <Link
              to={"/employees/detail/" + record.id}
              style={{ color: 'green' }}>
              <Icon type="eye" style={{ color: '#00FF00' }} />
            </Link>
            <Divider type="vertical" />

            <Link to={"employees/edit/" + record.id} style={{ color: 'blue' }} >
              <Icon type="edit" />
            </Link>
            <Divider type="vertical" />
            <Popconfirm
              title="Are you sure delete?"
              onConfirm={() => this.delete(record.key)}
              okType="danger"
            >
              <a style={{ color: '#ff3333' }}><Icon type="delete" /></a>
            </Popconfirm>
          </span>
        ),
        fixed: 'right'
      }
    ];

    let dataSource = this.props.employees;
    dataSource.map(d => {
      let uuid = uuidv4();
      d.key = uuid;
    })

    const perform = {
      create: "position:create",
      edit: "position:edit"
    }
    const newData = {
      title: "",
      description: ""
    }

    return (
      <div>
        <PageHeaderWrapper />
        <div className='twrap'>
          <h2>Employee</h2>
          <p>You can add Employee basic data by one after clicking the Add Create button and can see the employees' data in table.</p>
          <Link to="/employees/create">
            <Button style={{ color: 'white', backgroundColor: '#395D42',marginBottom:'30px',height:'50px' }} size='large'>Create New Employee</Button>
          </Link>
          <ScrollTable
            dataSource={dataSource}
            columns={columns}
            title="Employee List"
            perform={perform}
            newData={newData}
            getData={this.getAllEmployee}
            getdata={(id) => this.getEmployee(id)}
            editData={(data, id) => this.editEmployee(data, id)}
            createNewData={(data) => this.createNewEmployee(data)}
            deleteData={(id) => this.deleteEmployee(id)}
          />
        </div>

      </div>


    );
  }
}

function mapStateToProps(state) {
  return {
    lang: state.locale.lang,
    isSignedIn: state.auth.isSignedIn,
    roleid: state.auth.roleid,
    isloaded: state.loading.isloaded,
    employees: state.employee.list,
  };
}
export default connect(
  mapStateToProps,
  { fetchEmployee, postEmployee, putEmployee, deleteEmployee }
)(Employee);