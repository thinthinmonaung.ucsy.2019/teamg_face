import React from 'react'

import { Breadcrumb } from 'antd';
import { Button, Radio, Icon } from 'antd';
import { Upload, message } from 'antd';
import { Divider } from 'antd';
import { Input } from 'antd';
import { Col, Row, Select, InputNumber, DatePicker, AutoComplete, Cascader } from 'antd';
import { Form, Checkbox } from 'antd';
import history from '../../../router/history'
import { connect } from "react-redux";
import { fetchPosition } from '../../../actions/Position';
import { fetchDepartment } from '../../../actions/Department';
import api from 'apis';
import { noti } from 'utils/index';
const moment = require('moment');

const image = {
  width: '100px',
  height: '100px',
}
const apiUrl = "http://localhost:9991/"

const tailFormItemLayout = {
  wrapperCol: {
    xs: {
      span: 24,
      offset: 0,
    },
    sm: {
      span: 16,
      offset: 8,
    },
  },
};



function getBase64(img, callback) {
  const reader = new FileReader();
  reader.addEventListener('load', () => callback(reader.result));
  reader.readAsDataURL(img);
}

function beforeUpload(file) {
  const isJPG = file.type === 'image/jpeg';
  if (!isJPG) {
    message.error('You can only upload JPG file!');
  }
  const isLt2M = file.size / 1024 / 1024 < 2;
  if (!isLt2M) {
    message.error('Image must smaller than 2MB!');
  }
  return isJPG && isLt2M;
}





const InputGroup = Input.Group;
const { Option } = Select;

const formItemLayout = {
  labelCol: { span: 4 },
  wrapperCol: { span: 8 },
};



class Employee extends React.Component {

  constructor(props) {
    super(props)
    this.state = {
      file: null,
      id: this.props.match.params.id,
      data: [],
      preview: null,
      loading: false
    };
  }

  handleChange = info => {
    if (info.file.status === 'uploading') {
      this.setState({ loading: true });
      return;
    }
    if (info.file.status === 'done') {
      console.log(info.file);
      console.log(info);
      let preview = URL.createObjectURL(info.file.originFileObj);
      getBase64(info.file.originFileObj, (imageUrl) => {
        console.log("Hello", imageUrl);

        this.setState({
          preview: preview,
          file: imageUrl,
          loading: false,
        })
      });
    }
  };
  handleSubmit = e => {
    e.preventDefault();
    this.props.form.validateFieldsAndScroll((err, fieldsValue) => {
      if (!err) {
        const values = {
          ...fieldsValue,
          'dob': fieldsValue['dob'].format('YYYY-MM-DD'),
          'start_date': fieldsValue['start_date'].format('YYYY-MM-DD')
        }
        values.image = this.state.file
        console.log(values)
        api.put(`employees/${values.id}`, values).then((result) =>{
          console.log(result)
          this.props.history.push('/employees')
        noti('success', 'Successfully!', 'Employee has been updated successfully.')
        })
        
      } else {
        noti('error', 'Unsuccessfully!', 'Fail to update.')
      }
    });
  };

  componentDidMount() {

    console.log("Date", this.getData());

  }


  async  getData() {
    const response = await api.get(`employees/${this.state.id}`);
    if (response && response.status == 200) {
      let data = response.data.data;
      console.log(data);

      let imgUrl = data.image ? apiUrl + data.image : '';
      this.setState({ data: data, preview: imgUrl })
      this.setInitialValues()
    }
  }

  onChange = (e) => {
    let preview = URL.createObjectURL(e.target.files[0]);
    this.getBase64(e.target.files[0], (result) => {
      this.setState({ preview: preview, file: result })
    });
  }

  getBase64(file, cb) {
    let reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = function () {
      cb(reader.result)
    };
    reader.onerror = function (error) {
      console.log('Error: ', error);
    };
  }


  state = {
    size: 'large',
  };

  handleSizeChange = e => {
    this.setState({ size: e.target.value });
  };

  check = () => {
    this.props.form.validateFields(err => {
      if (!err) {
        console.info('success');
      }
    });
  };
  setInitialValues = () => {
    const data = this.state.data;
    const { form } = this.props;
    if (data)
      form.setFieldsValue({
        id: data.id,
        code: data.code,
        name: data.name,
        position_id: data.position_id,
        department_id: data.department_id,
        dob: moment(data.dob),
        start_date: moment(data.start_date),
        education: data.education,
        email: data.email,
        father_name: data.father_name,
        mother_name: data.mother_name,
        phone: data.phone,
        nric: data.nric,
        parmanent_address: data.parmanent_address,
        temporary_address: data.temporary_address,
        social_media_link: data.social_media_link
      });
  };


  btnCancel = () => {
    history.push('/employees')
  }

  render() {
    const uploadButton = (
      <div>
        <Icon type={this.state.loading ? 'loading' : 'plus'} />
        <div className="ant-upload-text">Upload</div>
      </div>
    );
    const imageUrl = this.state.preview;



    const { getFieldDecorator } = this.props.form;

    const prefixSelector = getFieldDecorator('prefixphone', {
      initialValue: '+95',
    })(
      <Select style={{ width: 70 }}>
        <Option value="+95">+95</Option>
        <Option value="+86">+86</Option>
        <Option value="+87">+87</Option>
      </Select>,
    );

    const renderPosition = (
      <Select style={{
        width: '100%', display: 'inline-block'
      }} placeholder="Please select position">
        {this.props.position.map(item => {
          return <Option value={item.id}>{item.name}</Option>
        })}
      </Select>
    )
    const renderDepartment = (
      <Select style={{
        width: '100%', display: 'inline-block'
      }} placeholder="Please select department">
        {this.props.department.map(item => {
          return <Option value={item.id}>{item.name}</Option>
        })}
      </Select>
    )

    return (
      <div>
        <Breadcrumb style={{ fontSize: '13px', fontWeight: 'bold' }}>
          <Breadcrumb.Item>
            <a href="/">Configuration</a>
          </Breadcrumb.Item>
          <Breadcrumb.Item>
            <a href="/employees">Employee</a>
          </Breadcrumb.Item>
          <Breadcrumb.Item>
            <a href="" style={{ color: 'green' }}>Edit Employee</a>
          </Breadcrumb.Item>
        </Breadcrumb>
        <div>
          <Form onSubmit={this.handleSubmit}>
            <Form.Item>
              {getFieldDecorator("id")(<Input type="hidden" />)}
            </Form.Item>
            <Form.Item>
              {getFieldDecorator("image", {
                rules: [
                  {
                    required: true,
                    message: "Please upload your image"
                  }
                ]
              })(
                <Upload
                  name="avatar"
                  listType="picture-card"
                  className="avatar-uploader"
                  showUploadList={false}
                  action="https://www.mocky.io/v2/5cc8019d300000980a055e76"
                  beforeUpload={beforeUpload}
                  onChange={this.handleChange}
                >
                  {imageUrl ? <img src={imageUrl} alt="avatar" style={image} /> : uploadButton}
                </Upload>
              )
              }
            </Form.Item>
          </Form>
        </div>
        <br />

        <div style={{ marginLeft: '5%' }}>
          <Col span={8}>
            <Form.Item style={{
              width: '80%',
            }} label="Code:">
              {getFieldDecorator('code', {
                rules: [
                  {
                    required: true,
                    message: 'Insert Please',
                  }]
              })(<Input style={{ marginLeft: '1%', display: 'inline-block' }} />)}
            </Form.Item>
          </Col>
          <Col span={8}>
            <Form.Item style={{
              width: '80%',
            }} label="Name:">

              {getFieldDecorator('name', {
                rules: [
                  {
                    required: true,
                    message: 'Insert Please',
                  }
                ]
              })(<Input style={{ marginRight: '75%', display: 'inline-block' }} />)}
            </Form.Item>
          </Col>
          <Col span={8}>
            <Form.Item style={{
              width: '80%',
            }} label="NRIC:">
              {getFieldDecorator('nric', {
                rules: [
                  {
                    required: true,
                    message: 'Insert Please',
                  }]
              })(<Input style={{ marginRight: '75%', display: 'inline-block' }} />)}
            </Form.Item>
          </Col>

        </div>


        <div style={{ marginLeft: '5%' }}>
          <Col span={8}>
            <Form.Item style={{
              width: '80%',
            }} label="Father Name:">
              {getFieldDecorator('father_name', {
                rules: [
                  {
                    required: true,
                    message: 'Insert Please',
                  }]
              })(<Input />)}
            </Form.Item>
          </Col>

          <Col span={8}>
            <Form.Item style={{
              width: '80%',
            }} label="Mother Name:">

              {getFieldDecorator('mother_name', {
                rules: [
                  {
                    required: true,
                    message: 'Insert Please',
                  }
                ]
              })(<Input />)}
            </Form.Item>
          </Col>
          <Col span={8}>
            <Form.Item style={{
              width: '80%',
            }} label="Date Of Birth">
              {
                getFieldDecorator('dob', {
                  rules: [{
                    required: true,
                    message: 'Please input your date of birth!'
                  }]
                })(
                  <DatePicker style={{
                    width: '100%',
                    display: 'inline-block',
                  }}
                    format="YYYY-MM-DD"
                    defaultValue={this.dob}
                  />)}
            </Form.Item>
          </Col>


        </div>

        <div style={{ marginLeft: '5%' }}>
          {/* <Form.Item style={{
            width: '20%',
            display: 'inline-block'
          }} label="Position:">
            {getFieldDecorator('position_id', {
              rules: [
                {
                  required: true,
                  message: 'Insert Please',
                }]
            })(<Input style={{ marginLeft: '1%', display: 'inline-block' }} />)}
          </Form.Item> */}
          {/* 
          <Form.Item style={{
            width: '20%',
            marginLeft: '12%',
            display: 'inline-block'
          }} label="Department:">

            {getFieldDecorator('department_id', {
              rules: [
                {
                  required: true,
                  message: 'Insert Please',
                }
              ]
            })(<Input style={{ marginRight: '75%', display: 'inline-block' }} />)}
          </Form.Item> */}
          <Col span={8}>
            <Form.Item style={{
              width: '80%', display: 'inline-block'
            }} label="Position:">
              {getFieldDecorator('position_id', {
                rules: [{ required: true, message: 'Please select position' }],
              })
                (renderPosition)
              }
            </Form.Item>
          </Col>
          <Col span={8}>
            <Form.Item style={{
              width: '80%', display: 'inline-block'
            }} label="Department:">
              {getFieldDecorator('department_id', {
                rules: [{ required: true, message: 'Please select department' }],
              })
                (renderDepartment)}
            </Form.Item>
          </Col>


          <Col span={8}>
            <Form.Item style={{
              width: '80%'
            }} label="Start Date:">
              {getFieldDecorator('start_date', {
                rules: [
                  {
                    required: true,
                    message: 'Insert Please',
                  }]
              })(
                <DatePicker style={{
                  width: '100%',
                  marginRight: '75%',
                  display: 'inline-block',
                }} placeholder="dd/mm/yyyy"
                  format="YYYY-MM-DD"
                  defaultValue={this.start_date}
                />
              )}
            </Form.Item>
          </Col>

        </div>

        <div style={{ marginLeft: '5%' }}>
<Col span={8}>
<Form.Item style={{
            width: '80%',
          }} label="Email:">
            {getFieldDecorator('email', {
              rules: [
                {
                  required: true,
                  message: 'Insert Please',
                }]
            })(<Input />)}
          </Form.Item>
  </Col>

 <Col span={8}>
 <Form.Item style={{
            width: '80%',
          }} label="Education:">
            {getFieldDecorator('education', {
              rules: [
                {
                  required: true,
                  message: 'Insert Please',
                }]
            })(<Input />)}
          </Form.Item>
          </Col>


<Col span={8}>
<Form.Item style={{
            width: '80%',
            display: 'inline-block'
          }} label="Phone No:">
            {getFieldDecorator('phone', {
              rules: [
                {
                  required: true,
                  message: 'Insert Please',
                }]
            })(<Input addonBefore={prefixSelector}  />)}
          </Form.Item>
  </Col>

        </div>

        <div style={{ marginLeft: '5%' }}>
 <Col span={8}>
 <Form.Item style={{
            width: '80%',
            display: 'inline-block'
          }} label="Parment Address:">

            {getFieldDecorator('parmanent_address', {
              rules: [
                {
                  required: true,
                  message: 'Insert Please ',
                }
              ]
            })(<Input style={{ marginLeft: '1%', display: 'inline-block' }} />)}
          </Form.Item>
 </Col>

 <Col span={8}>
 <Form.Item style={{
            width: '80%',
          }} label="Temporary Address:">

            {getFieldDecorator('temporary_address', {
              rules: [
                {
                  required: true,
                  message: 'Insert Please ',
                }
              ]
            })(<Input/>)}
          </Form.Item>
 </Col>
<Form.Item style={{
            width: '27%',
            display: 'inline-block'
          }} label="Social Media Link:">
            {getFieldDecorator('social_media_link', {
              rules: [
                {
                  required: true,
                  message: 'Insert Please',
                }]
            })(<Input/>)}
          </Form.Item>

        </div>
        <div>
          <Form {...formItemLayout} onSubmit={this.handleSubmit}>
            <Form.Item {...tailFormItemLayout} wrapperCol={{ span: 24 }} >
              <Button type="primary" htmlType="submit" style={{ marginLeft: '5%', backgroundColor: "green", color: "white", borderColor: "green" }}>
                Update
                        </Button>

              <Button type="primary" style={{ marginLeft: '2%', backgroundColor: "white", color: "black", borderColor: "gray" }} onClick={this.btnCancel} >
                Cancel
                    </Button>
            </Form.Item>
          </Form>
        </div>

      </div>
    );
  }
}

const Employee1 = Form.create()(Employee);

function mapStateToProps(state) {
  return {
    lang: state.locale.lang,
    isSignedIn: state.auth.isSignedIn,
    roleid: state.auth.roleid,
    isloaded: state.loading.isloaded,
    position: state.position.list,
    department: state.department.list
  };
}


export default connect(
  mapStateToProps,
  { fetchPosition, fetchDepartment }
)(Employee1);