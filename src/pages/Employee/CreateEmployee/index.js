import React from 'react'

import {
    Form,
    Breadcrumb,
    Button, Icon,
    Upload, message, Divider,
    Input, Select, DatePicker, Row, Col
} from 'antd';
import { connect } from "react-redux";

import api from 'apis';
import { noti } from 'utils/index';
import PageHeaderWrapper from '../../../components/PageHeaderWrapper';
import { fetchEmployee, postEmployee } from '../../../actions/Employee';
import { fetchPosition } from '../../../actions/Position';
import { fetchDepartment } from '../../../actions/Department';
import history from '../../../router/history'

import { Link } from 'react-router-dom'
import './index.css';

const image = {
    width: '100px',
    height: '100px',
}

function getBase64(img, callback) {
    const reader = new FileReader();
    reader.addEventListener('load', () => callback(reader.result));
    reader.readAsDataURL(img);
}

function beforeUpload(file) {
    const isJPG = file.type === 'image/jpeg';
    if (!isJPG) {
        message.error('You can only upload JPG file!');
    }
    const isLt2M = file.size / 1024 / 1024 < 2;
    if (!isLt2M) {
        message.error('Image must smaller than 2MB!');
    }
    return isJPG && isLt2M;
}


const { Option } = Select;
class CreateEmployee extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            file: null,
            loading: false,
        };
    }

    componentDidMount() {
        this.getAllData();
    }

    getAllData() {
        this.props.fetchDepartment();
        this.props.fetchPosition();
    }



    handleChange = info => {
        if (info.file.status === 'uploading') {
            this.setState({ loading: true });
            return;
        }
        if (info.file.status === 'done') {
            console.log(info.file);
            console.log(info);
            let preview = URL.createObjectURL(info.file.originFileObj);
            // Get this url from response in real world.
            getBase64(info.file.originFileObj, (imageUrl) => {
                console.log("Hello", imageUrl);

                this.setState({
                    preview: preview,
                    file: imageUrl,
                    loading: false,
                })
            });
        }
    };


    state = {
        size: 'large',
    };

    handleSizeChange = e => {
        this.setState({ size: e.target.value });
    };




    handleSubmit = e => {
        e.preventDefault();
        this.props.form.validateFieldsAndScroll((err, fieldsValue) => {
            if (!err) {
                const values = {
                    ...fieldsValue,
                    'dob': fieldsValue['dob'].format('YYYY-MM-DD'),
                    'start_date': fieldsValue['start_date'].format('YYYY-MM-DD')
                }
                values.image = this.state.file
                api.post('employees', values).then((result) => {

                    console.log("Result", result);
                    this.props.history.push('/employees')
                    noti('success', 'Successfully!', 'Employee has been created successfully.')
                })
                
            }
            
             else {
                noti('error', 'Unsuccessfully!', 'Fail to Create.')
            }
        });
    };

btnCancel=()=>{
    history.push('/employees')
}

    render() {
        const uploadButton = (
            <div>
                <Icon type={this.state.loading ? 'loading' : 'plus'} />
                <div className="ant-upload-text">Upload</div>
            </div>
        );
        const imageUrl = this.state.preview;
        const size = this.state.size;

        const { getFieldDecorator } = this.props.form;

        const renderPosition = (
            <Select style={{
                width: '300px',
                marginleft: '10px',
                display: 'inline-block'
            }} placeholder="Please select position">
                {this.props.position.map(item => {
                    return <Option value={item.id}>{item.name}</Option>
                })}
            </Select>
        )
        const renderDepartment = (
            <Select style={{
                width: '300px',
                marginleft: '10px',
                display: 'inline-block'
            }} placeholder="Please select department">
                {this.props.department.map(item => {
                    return <Option value={item.id}>{item.name}</Option>
                })}
            </Select>
        )
        const prefixSelector = getFieldDecorator('prefixphone', {
            initialValue: '+95',
        })(
            <Select style={{ width: 70 }}>
                <Option value="+95">+95</Option>
                <Option value="+86">+86</Option>
                <Option value="+87">+87</Option>
            </Select>,
        );



        return (
            <div>
                <PageHeaderWrapper />
                <h2>Create Employee</h2>
                <p> You can add Employee basic data by entering one by one using the following form.</p>
                <br />

                <Form onSubmit={this.handleSubmit} style={{ border: '1px solid gray', borderRadius: '3px' }} >
                    <div style={{ margin: '3%' }}>
                        <Form.Item>
                            {getFieldDecorator("image", {
                                rules: [
                                    {
                                        required: false,
                                        message: "Please upload your image"
                                    }
                                ]
                            })(
                                <Upload
                                    name="avatar"
                                    listType="picture-card"
                                    className="avatar-uploader"
                                    showUploadList={false}
                                    action="https://www.mocky.io/v2/5cc8019d300000980a055e76"
                                    beforeUpload={beforeUpload}
                                    onChange={this.handleChange}
                                >
                                    {imageUrl ? <img src={imageUrl} alt="avatar" style={image} /> : uploadButton}
                                </Upload>

                                // <input type="file" name="image" onChange={this.onChange} />
                            )
                            }
                        </Form.Item>
                        <br />

                        <div style={{ marginBottom: '3%' }}>
                            <Row>
                                <h2><Divider orientation="left">  <Icon type="idcard" style={{ fontSize: '30px', marginLeft: '5px' }} />Personal Information</Divider></h2>
                                <Col span={12}>
                                    <Form.Item style={{
                                        width: '60%',
                                    }} label="Name:">
                                        {getFieldDecorator('name', {
                                            rules: [{
                                                required: true,
                                                message: 'Please input your name!'
                                            }]
                                        })(<Input placeholder="Enter Name" />)}
                                    </Form.Item>
                                </Col>

                                <Col span={12}>
                                    <Form.Item style={{
                                        width: '60%',
                                    }} label="NRIC:">
                                        {getFieldDecorator('nric', {
                                            rules: [{
                                                required: true,
                                                message: 'Please input your NRIC!'
                                            }]
                                        })(<Input placeholder="Enter NRIC" />)}
                                    </Form.Item>
                                </Col>
                            </Row>
                            <Row>
                                <Col span={12}>
                                    <Form.Item style={{
                                        width: '60%',
                                    }} label="Date Of Birth">
                                        {
                                            getFieldDecorator('dob', {
                                                rules: [{
                                                    required: true,
                                                    message: 'Please input your date of birth!'
                                                }]
                                            })(<DatePicker placeholder="dd/mm/yyyy" />)}
                                    </Form.Item>
                                </Col>
                                <Col span={12}>
                                    <Form.Item style={{
                                        width: '60%'
                                    }} label="Employee Code">
                                        {getFieldDecorator('code', {
                                            rules: [{
                                                required: true,
                                                message: 'Please input your code!'
                                            }]
                                        })(<Input placeholder="Enter Code" />)}
                                    </Form.Item>
                                </Col>
                            </Row>
                        </div>


                        <div style={{ marginBottom: '3%' }}>
                            <Row>
                                <h2> <Divider orientation="left"> <Icon type="shopping" style={{ fontSize: '30px', marginLeft: '3%' }} /> Job Information</Divider></h2>

                                <Col span={12}>
                                    <Form.Item style={{
                                        width: '60%'
                                    }} label="Position:">
                                        {getFieldDecorator('position_id', {
                                            rules: [{ required: true, message: 'Please select position' }],
                                        })
                                            (renderPosition)
                                        }
                                    </Form.Item>
                                </Col>
                                <Col span={12}>
                                    <Form.Item style={{
                                        width: '60%'
                                    }} label="Department:">
                                        {getFieldDecorator('department_id', {
                                            rules: [{ required: true, message: 'Please select department' }],
                                        })
                                            (renderDepartment)}
                                    </Form.Item>
                                </Col>
                            </Row>
                            <Row>
                                <Col span={24}>

                                    <Form.Item style={{
                                        width: '30%'
                                    }} label="Start Date:">
                                        {getFieldDecorator('start_date', {
                                            rules: [{
                                                required: true,
                                                message: 'Please input your start date!'
                                            }]
                                        })(
                                            <DatePicker
                                                dateRender={current => {
                                                    const style = {};
                                                    if (current.date() === 1) {
                                                        style.border = '1px solid #1890ff';
                                                        style.borderRadius = '50%';
                                                    }
                                                    return (
                                                        <div className="ant-calendar-date" style={style}>
                                                            {current.date()}
                                                        </div>
                                                    );
                                                }}
                                            />
                                        )}
                                    </Form.Item>
                                </Col>
                            </Row>
                        </div>

                        <div style={{ marginBottom: '3%' }}>
                            <Row>
                                <h2> <Divider orientation="left"> <Icon type="contacts" style={{ fontSize: '30px', marginLeft: '3%' }} /> Contact Information</Divider></h2>
                                <Col span={12}>
                                <Form.Item style={{
                                width: '60%',
                            }} label="Email:">
                                {getFieldDecorator('email', {
                                    rules: [{
                                        required: true,
                                        message: 'Please input your email!'
                                    }]
                                })(<Input placeholder="Enter Email" />)}
                            </Form.Item>
                                </Col>
                                <Col span={12}>
                                    <Form.Item
                                        style={{
                                            width: '60%'
                                        }}
                                        label="Phone Number">
                                        {getFieldDecorator('phone', {
                                            rules: [{ required: true, message: 'Please input your phone number!' }],
                                        })(<Input
                                            placeholder="777777777"
                                            addonBefore={prefixSelector} style={{
                                                width: '100%',
                                            }} />)}
                                    </Form.Item>
                                </Col>
                            </Row>

                            <Row>
                                <Col span={12}>

                                    <Form.Item style={{
                                        width: '60%'
                                    }} label="Parment Address:">
                                        {getFieldDecorator('parmanent_address', {
                                            rules: [{
                                                required: true,
                                                message: 'Please input your parment address!'
                                            }]
                                        })(<Input placeholder="Enter Parment Address" />)}
                                    </Form.Item>
                                </Col>
                                <Col span={12}>
                                    <Form.Item style={{
                                        width: '60%'
                                    }} label="Temporary Address:">
                                        {getFieldDecorator('temporary_address', {
                                            rules: [{
                                                required: true,
                                                message: 'Please input your temporary address!'
                                            }]
                                        })(<Input placeholder="Enter Temporary Address" />)}
                                    </Form.Item>
                                </Col>
                            </Row>
                        </div>

                        <div style={{ marginBottom: '3%' }}>
                            <Row>
                                <h2> <Divider orientation="left"> <Icon type="team" style={{ fontSize: '30px', marginLeft: '3%' }} /> Parent Information</Divider></h2>
                                <Col span={12}>
                                    <Form.Item style={{
                                        width: '60%'
                                    }} label="Father Name:">
                                        {getFieldDecorator('father_name', {
                                            rules: [{
                                                required: true,
                                                message: 'Please input your father name!'
                                            }]
                                        })(<Input placeholder="Enter Father Name" />)}
                                    </Form.Item>
                                </Col>
                                <Col span={12}>
                                    <Form.Item style={{
                                        width: '60%',
                                    }} label="Mother Name:">
                                        {getFieldDecorator('mother_name', {
                                            rules: [{
                                                required: true,
                                                message: 'Please input your mother name!'
                                            }]
                                        })(<Input placeholder="Enter Mother Name" />)}
                                    </Form.Item>
                                </Col>
                            </Row>
                        </div>


                        <div>
                            <Row>
                                <h2> <Divider orientation="left"> <Icon type="info-circle" style={{ fontSize: '30px', marginLeft: '3%' }} /> Other Information</Divider></h2>
                                <Col span={12}>
                                    <Form.Item style={{
                                        width: '60%'
                                    }} label="Education">
                                        {getFieldDecorator('education', {
                                            rules: [{
                                                required: true,
                                                message: 'Please input your education address!'
                                            }]
                                        })(<Input placeholder="Enter education Address" />)}
                                    </Form.Item>
                                </Col>
                                <Col span={12}>
                                    <Form.Item style={{
                                        width: '60%',
                                    }} label="Social_Media_Link">
                                        {getFieldDecorator('social_media_link', {
                                            rules: [{
                                                required: true,
                                                message: 'Please input your social_media_link address!'
                                            }]
                                        })(<Input placeholder="Enter social_media_link Address" />)}
                                    </Form.Item>
                                </Col>
                            </Row>
                        </div>
                        <Row>
                            <Form.Item wrapperCol={{ span: 12 }}>
                                <Button htmlType="submit" style={{ color: 'white', backgroundColor: 'green', borderColor: 'green' }}> Submit</Button>
                                <Button onClick={this.btnCancel} style={{ marginLeft: '15px', color: 'black', backgroundColor: 'white', borderColor: 'gray' }}> Cancel</Button>
                            </Form.Item>
                        </Row>
                    </div>
                </Form>
            </div >



        );
    }
}


const Employee = Form.create()(CreateEmployee);

function mapStateToProps(state) {
    return {
        lang: state.locale.lang,
        isSignedIn: state.auth.isSignedIn,
        roleid: state.auth.roleid,
        isloaded: state.loading.isloaded,
        position: state.position.list,
        department: state.department.list
    };
}

export default connect(
    mapStateToProps,
    { fetchEmployee, postEmployee, fetchPosition, fetchDepartment }
)(Employee);
