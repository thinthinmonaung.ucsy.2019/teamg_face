import requiredAllAuth from '../components/hocs/AllAuth'
import Layout from '../layout'
import Dashboard from '../pages/Dashboard'
import Login from '../pages/Login'
import Position from '../pages/Position'
import Model from '../pages/Model'
import Notfound from '../pages/Notfound'
import Machine from '../pages/Machine'
import Department from '../pages/Department';
import DashComplain from '../pages/Dashboard/dashcomplain'

import Employee from '../pages/Employee';
import CreateEmployee from '../pages/Employee/CreateEmployee';
import EditEmployee from '../pages/Employee/EditEmployee';
import DetailEmployee from '../pages/Employee/DetailEmployee';

import ViewMachine from '../pages/Machine/ViewMachine';
import CreateNewMachine from '../pages/Machine/CreateNewMachine';
import EditMachine from '../pages/Machine/EditMachine';
import Complain from '../pages/Complain';
import DetailComplain from '../pages/Complain/DetailComplain'
import CreateComplain from '../pages/Complain/CreateComplain'
import EditComplain from '../pages/Complain/EditComplain';

import AssignSchedule from '../pages/AssignSchedule';
import AssignScheduleView from '../pages/AssignSchedule/AssingScheduleView';
import RejectView from '../pages/AssignSchedule/AssingScheduleView/rejectview';
import AssignScheduleAccept from '../pages/AssignSchedule/AssignScheduleAccept';
import AssignMachineView from '../pages/AssignSchedule/Machineview'

//Schedule
import Schedule from '../pages/Schedule';
import DetailSchedule from '../pages/Schedule/DetailSchedule';
import EditSchedule from '../pages/Schedule/EditSchedule';

//Job
import Job from '../pages/Job';
import DetailJob from '../pages/Job/DetailJob';
import JobDone from '../pages/Job/JobDone';

import Terms from '../pages/Terms'

import Role from '../pages/Role';

import Module from '../pages/Module'
import ModuleCreate from '../pages/Module/Upload/upload'
import ModuleView from '../pages/Module/View/view'
import ModuleEdit from '../pages/Module/Upload/edit'

import Account from '../pages/Account'
import AccountCreate from '../pages/Account/Upload/upload'
import AccountView from '../pages/Account/View/view'
import AccountEdit from '../pages/Account/Upload/edit'

import Profile from '../pages/Profile'
import ProEdit from '../pages/Profile/edit'

//report
import Daily from '../pages/Daily'
import CreateDaily from '../pages/Daily/create'
import ReportView from '../pages/Daily/view'

export default [
    {
        component: Layout,
        routes: [
            {
                component: requiredAllAuth(Dashboard),
                path: '/',
                exact: true
            },
            {
                component: requiredAllAuth(DashComplain),
                path: '/dashcomplain',
                exact: true
            },
            {
                component: Login,
                path: '/login',
            },
            //Position Route
            {
                component: requiredAllAuth(Position),
                path: '/positions',
                exact: true
            },
            //Department Route
            {
                component: requiredAllAuth(Department),
                path: '/departments',
                exact: true
            },


            //Employee Route
            {
                component: requiredAllAuth(Employee),
                path: '/employees',
                exact: true
            },
            {
                component: requiredAllAuth(CreateEmployee),
                path: '/employees/create',
                exact: true
            },
            {
                component: requiredAllAuth(DetailEmployee),
                path: '/employees/detail/:id',
                exact: true
            },
            {
                component: requiredAllAuth(EditEmployee),
                path: '/employees/edit/:id',
                exact: true
            },
            //Model Route
            {
                component: requiredAllAuth(Model),
                path: '/models',
                exact: true
            },

            //Machine Route
            {
                component: requiredAllAuth(Machine),
                path: '/machines',
                exact: true,

            },
            {
                component: requiredAllAuth(ViewMachine),
                path: '/machines/detail/:id',
                exact: true
            },
            {
                component: requiredAllAuth(EditMachine),
                path: '/machines/edit/:id',
                exact: true
            },
            {
                component: requiredAllAuth(CreateNewMachine),
                path: '/machines/create',
                exact: true
            },


            {
                component: requiredAllAuth(AssignMachineView),
                path: '/assigntoschedule/machineview/:id',
                exact: true
            },
            //Complain Route
            {
                component: requiredAllAuth(Complain),
                path: '/complains',
                exact: true
            },
            {
                component: requiredAllAuth(DetailComplain),
                path: '/complains/detail/:id',
                exact: true
            },
            {
                component: requiredAllAuth(EditComplain),
                path: '/complains/edit/:id',
                exact: true
            },
            {
                component: requiredAllAuth(CreateComplain),
                path: '/complains/create',
                exact: true
            },

            //AssingScheduleView
            {
                component: requiredAllAuth(AssignSchedule),
                path: '/assign to schedule',
                exact: true
            },
            {
                component: requiredAllAuth(AssignScheduleView),
                path: '/assign to schedule/detail/:id',
                exact: true
            },
            {
                component: requiredAllAuth(RejectView),
                path: '/assign to schedule/rejectview/:id',
                exact: true
            },
            {
                component: requiredAllAuth(AssignScheduleAccept),
                path: '/assign to schedule/accept/:id',
                exact: true
            },
            //Schedule 
            {
                component: requiredAllAuth(Schedule),
                path: '/schedules',
                exact: true
            },

            {
                component: requiredAllAuth(DetailSchedule),
                path: '/schedules/detail/:id',
                exact: true
            },
            {
                component: requiredAllAuth(EditSchedule),
                path: '/schedules/edit/:id',
                exact: true
            },
            {
                component: requiredAllAuth(Account),
                path: '/account',
                exact: true
            },
            {
                component: requiredAllAuth(AccountCreate),
                path: '/account/create',
                exact: true
            },
            {
                component: requiredAllAuth(AccountView),
                path: '/account/view/:id',
                exact: true
            },
            {
                component: requiredAllAuth(AccountEdit),
                path: '/account/upload/edit/:id',
                exact: true
            },
            {
                component: requiredAllAuth(Module),
                path: '/module',
                exact: true
            },
            {
                component: requiredAllAuth(Role),
                path: '/role',
                exact: true
            },
            {
                component: requiredAllAuth(ModuleCreate),
                path: '/module/create',
                exact: true
            },
            {
                component: requiredAllAuth(ModuleView),
                path: '/module/view/:id',
                exact: true
            },
            {
                component: requiredAllAuth(ModuleEdit),
                path: '/module/:id',
                exact: true
            },
            {
                component: requiredAllAuth(Profile),
                // path: '/profile/:id',
                path: '/profile',
                exact: true
            },
            {
                component: requiredAllAuth(ProEdit),
                path: '/proedit',
                exact: true
            },
            {
                component: requiredAllAuth(Daily),
                path: '/report',
                exact: true
            },
            {
                component: requiredAllAuth(CreateDaily),
                path: '/report/create',
                exact: true
            },
            {
                component: requiredAllAuth(ReportView),
                path: '/report/view/:id',
                exact: true
            },
            {
                component: requiredAllAuth(Job),
                path: '/job',
                exact: true
            },
            {
                component: requiredAllAuth(DetailJob),
                path: '/Job/detail/:id',
                exact: true
            },
            {
                component: requiredAllAuth(JobDone),
                path: '/Job/jobdone/:id',
                exact: true
            },
            {
                component: requiredAllAuth(Terms),
                path: '/terms and conditions',
                exact: true
            },
            {
                component: Notfound
            }
        ]
    }
]