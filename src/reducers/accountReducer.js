import {
    CREATE_ACCOUNT,
    FETCH_ACCOUNT,
    EDIT_ACCOUNT,
    DELETE_ACCOUNT,
    FETCH_ACCOUNTS
  } from "../actions/types";
  import _ from "lodash";
  const INTIAL_STATE = {
    list:[],
    account:{}
  };
  
  export default (state = INTIAL_STATE, action) => {
    switch (action.type) {
      case FETCH_ACCOUNTS:
        return { ...state, list:action.payload, };
      case FETCH_ACCOUNT:
        return { ...state, [action.payload.id]: action.payload };
      case CREATE_ACCOUNT:
        return { ...state, [action.payload.id]: action.payload };
      case EDIT_ACCOUNT:
        return { ...state, [action.payload.id]: action.payload };
      case DELETE_ACCOUNT:
        return _.omit(state, action.payload);
      default:
        return state;
    }
  };
  