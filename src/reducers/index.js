import { combineReducers } from 'redux';
// import { reducer as formReducer } from 'redux-form';
import authReducer from './authReducer';
import streamReducer from './streamReducer';
import locale from './localeReducer'
import positionReducer from './postionReducer';
import modelReducer from './modelReducer';
import loadingReducer from './loadingReducer'
import departmentReducer from './departmentReducer';
import machineReducer from './machineReducer';
import scheduleReducer from './scheduleReducer';
import employeeReducer from './employeeReducer';
import serviceReducer from './serviceReducer';
import complainReducer from './complainReducer';
import accountReducer from './accountReducer';
import moduleReducer from './moduleReducer';
import roleReducer from './roleReducer';
import reportReducer from './reportReducer';
export default combineReducers({
    auth: authReducer,
    streams: streamReducer,
    position: positionReducer,
    department: departmentReducer,
    model: modelReducer,
    machine: machineReducer,
    employee: employeeReducer,
    schedule: scheduleReducer,
    service: serviceReducer,
    complain: complainReducer,
    account:accountReducer,
    module: moduleReducer, 
    role : roleReducer,
    report: reportReducer,

    locale,
    loading: loadingReducer
});
